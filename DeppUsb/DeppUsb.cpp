/*
 * DeppUsb.cpp
 *
 *  Created on: Sep 20, 2016
 *      Author:
 *      		Paris Moschovakos <paris.moschovakos@cern.ch>
 *              Aimilianos Koulouris <aimilianos.koulouris@gmail.com>
 *              Piotr Nikiel <piotr.nikiel@cern.ch>
 */

#include <stdexcept>
#include <LogIt.h>
#include <DeppUsb/DeppUsb.h>
#include <depp.h>
#include <dmgr.h>

namespace DeppUsb
{
    DeppUsb::DeppUsb( const std::string& deviceName, bool verbose )
    {
	char dmgrVersion [cchVersionMax];
	DmgrGetVersion( dmgrVersion );
	LOG(Log::INF) << "DMGR version: " << dmgrVersion;

	if (!DmgrOpen( &m_hif, const_cast<char*>(deviceName.c_str())))
	{
		throwLastError("DmgrOpen failed: ");
	}

	LOG(Log::INF) << "DMGR opened. Interface handle=" << m_hif;

	if (verbose) {
		DVC		dvc;
		char	szTmp[1024];
		DCAP	dcapTmp;
		DWORD	pdidTmp;
		DWORD	dwTmp;

		if (!findDvc(const_cast<char*>(deviceName.c_str()), &dvc))
			throw std::runtime_error("Device not found!");

		if (!DmgrGetInfo(&dvc, dinfoProdName, szTmp))
			throw std::runtime_error("Get device info failed");
		LOG(Log::INF) << "Device info: Product Name: " << szTmp;

		if (!DmgrGetInfo(&dvc, dinfoUsrName, szTmp))
			throw std::runtime_error("Get device info failed");
		LOG(Log::INF) << "Device info: Username: " << szTmp;

		if (!DmgrGetInfo(&dvc, dinfoSN, szTmp))
			throw std::runtime_error("Get device info failed");
		LOG(Log::INF) << "Device info: Serial Number: " << szTmp;

		if (!DmgrGetInfo(&dvc, dinfoPDID, &pdidTmp))
			throw std::runtime_error("Get device info failed");
		LOG(Log::INF) << "Device info: Product ID: " << pdidTmp;

		if (!DmgrGetInfo(&dvc, dinfoDCAP, &dcapTmp))
			throw std::runtime_error("Get device info failed");
		LOG(Log::INF) << "Device info: Device Capabilities: " << dcapTmp;

		if ((dcapTmp & dcapJtag) != 0)
			LOG(Log::INF) << "Device info: DJTG - JTAG scan chain access";
		if ((dcapTmp & dcapPio) != 0)
			LOG(Log::INF) << "Device info: DPIO - Digital Pin Input/Output";
		if ((dcapTmp & dcapEpp) != 0)
			LOG(Log::INF) << "Device info: DEPP - Asynchronous Parallel Input/Output";
		if ((dcapTmp & dcapStm) != 0)
			LOG(Log::INF) << "Device info: DSTM - Streaming Synchronous Parallel Input/Output";
		if ((dcapTmp & dcapSpi) != 0)
			LOG(Log::INF) << "Device info: DSPI - Serial Peripheral Interface";
		if ((dcapTmp & dcapTwi) != 0)
			LOG(Log::INF) << "Device info: DTWI - Two Wire Serial Interface";
		if ((dcapTmp & dcapAci) != 0)
			LOG(Log::INF) << "Device info: DACI - Asynchronous Serial Interface";
		if ((dcapTmp & dcapAio) != 0)
			LOG(Log::INF) << "Device info: DAIO - Analog Input/Output Interface";
		if ((dcapTmp & dcapEmc) != 0)
			LOG(Log::INF) << "Device info: DEMC - Electro-Mechanical Interface";
		if ((dcapTmp & dcapGio) != 0)
			LOG(Log::INF) << "Device info: DGIO - General Sensor and U/I Device Interface";

		dwTmp = 0;
		if (DmgrGetInfo(&dvc, dinfoFWVER, &dwTmp))
			LOG(Log::INF) << "Device info: Firmware Version: " << dwTmp;
	}

	char deppVersion [cchVersionMax];
	DeppGetVersion( deppVersion );
	LOG(Log::INF) << "DEPP version: " << deppVersion;
	if (!DeppEnable( m_hif ))
	{
		throwLastError("DeppEnable failed: ");
	}
	LOG(Log::INF) << "DEPP enabled";

    if (!DeppPutReg( m_hif, 0xff, 0, false))
    {
    	throwLastError("Communication with the FPGA firmware failed: ");
    }
    }

    DeppUsb::~DeppUsb() {
    	if ( m_hif )
    	{
    		DeppDisable( m_hif );
    		DmgrClose( m_hif );
    	}
    	m_hif = hifInvalid ;
    	LOG(Log::INF) << "DmgrClosed...";
    }

    void DeppUsb::deppWrite(uint8_t address, uint8_t data, bool overlap) 
    {
	boost::lock_guard<boost::mutex> lock( m_single_access_mutex );
	// LOG(Log::INF) << "deppWrite addr=" << std::hex << (unsigned int)address << " data=" << (unsigned int)data;
	bool rv = DeppPutReg (m_hif, address, data, overlap);
	if (!rv)
	{

		throwLastError("DeppPutReg failed (is FPGA firmware loaded?): ");
	}
    }

    uint8_t DeppUsb::deppRead( uint8_t address, bool overlap)
    {
	boost::lock_guard<boost::mutex> lock( m_single_access_mutex );
	uint8_t data;
	bool rv = DeppGetReg(m_hif, address, &data, overlap);
	// LOG(Log::INF) << "deppRead addr=" << std::hex << (unsigned int)address << " data=" << (unsigned int)data;
	if (!rv)
	{

		throwLastError("DeppGetReg failed (is FPGA firmware loaded?): ");
	}
	return data;
    }

    void DeppUsb::deppWriteMany( const std::vector< std::pair<uint8_t, uint8_t > > deppDataPair, bool overlap )
    {
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex );

	int arSize = deppDataPair.size();
	uint8_t dataPairs[arSize];
    for ( auto i = 0; i < deppDataPair.size(); i++ ) {
    	dataPairs[2 * i] = deppDataPair[i].first;
    	dataPairs[2 * i + 1] = deppDataPair[i].second;
    }

    uint8_t* addrDataPairs = dataPairs;
	bool rv = DeppPutRegSet (m_hif, addrDataPairs, deppDataPair.size(), overlap);
	if (!rv)
	{
		throwLastError("DeppPutRegSet failed. Could not send data to the FPGA. ");
	}
    }

    bool DeppUsb::findDvc(char * szDev, DVC * pdvc) {

    	int		idvc;
    	int		cdvc;
    	bool	fRes;

    	/* Find the device to use to run the test.
    	*/
    	fRes = fFalse;

    	// DMGR API Call: DmgrEnumDevices
    	DmgrEnumDevices(&cdvc);

    	for (idvc = 0; idvc < cdvc; idvc++) {
    		// DMGR API Call: DmgrGetDvc
    		DmgrGetDvc(idvc, pdvc);
    		if (strcmp(pdvc->szName, szDev) == 0) {
    			fRes = fTrue;
    			break;
    		}
    	}

    	// DMGR API Call: DmgrFreeDvcEnum
    	DmgrFreeDvcEnum();
    	return fRes;

    }

    void DeppUsb::throwLastError(std::string userMessage) {
	    auto errorCode = DmgrGetLastError();
	    char explanation[cchErcMax], message[cchErcMsgMax];
	    DmgrSzFromErc( errorCode, explanation, message );
	    throw std::runtime_error(userMessage + explanation + ": " + message);
    }
} /* namespace DeppUsb */
