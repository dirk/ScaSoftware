#include <DeppUsb/WishboneController.h>

#include <LogIt.h>

namespace DeppUsb
{

    enum WishboneDeppAddresses
    {
	WB_CMD = 0x0,
	WB_INTV = 0x1,
	WB_ADR0 = 0x8,
	WB_ADR1 = 0x9,
	WB_ADR2 = 0xa,
	WB_ADR3 = 0xb,
	WB_DAT0 = 0xc,
	WB_DAT1 = 0xd,
	WB_DAT2 = 0xe,
	WB_DAT3 = 0xf
    };

    WishboneController::WishboneController( DeppUsb * deppusb_instance, bool fast ):
	m_deppusb( deppusb_instance ), m_fast(true)
    {

    }

    void WishboneController::write( uint8_t device, uint32_t address, uint32_t data)
    {
    if (m_fast)
    	writeFast(device, address, data);
    else
    {
    	LOG(Log::INF) << "+ wishbone write: dev=" << std::hex << (unsigned int)device << " addr=" << address << " data=" << data << " ";
    	// TODO: Paris, Emil: this is completely unoptimized write, as seen in Alessandro work probably current device and address could be cached
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR3, device );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR2, (address>>16) & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR1, (address>> 8) & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR0, address & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_DAT0, data & 0xff ); // TODO: how to then use this data?
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_DAT1, (data >> 8) & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_DAT2, (data >>16) & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_DAT3, (data >>24) & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_CMD, 0x01 ); // TODO: AC: could this 0x1 by a constant ... ?
    	LOG(Log::INF) << "- wishbone write";
    }
    }

    uint32_t WishboneController::read( uint8_t device, uint32_t address)
    {
    if (m_fast)
    	readFast(device, address);
    else
    {
    	LOG(Log::INF) << "+ wishbone read: dev=" << std::hex << (unsigned int)device << " addr=" << address << std::dec;
    	// TODO: optimize for curdev and curadde as in AC?
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR3, device );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_ADR0, address & 0xff );
    	m_deppusb->deppWrite( WishboneDeppAddresses::WB_CMD, 0x02 ); // TODO: AC: could this be a constant?
    	uint32_t read_data = 0;
    	read_data |= m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT0 );
    	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT1 )) << 8;
    	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT2 )) << 16;
    	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT3 )) << 24;
    	LOG(Log::INF) << "- wishbone read: data=" << std::hex << read_data << std::dec;
    	return read_data;
    }
    }

    void WishboneController::writeFast( uint8_t device, uint32_t address, uint32_t data )
    {
	LOG(Log::INF) << "+ wishbone write fast: dev=" << std::hex << (unsigned int)device << " addr=" << address << " data=" << data << " ";

	std::vector< std::pair<uint8_t, uint8_t > > wbDeppPair;

	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_ADR3, device) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_ADR0, address & 0xff ) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_DAT0, data & 0xff ) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_DAT1, (data >> 8) & 0xff ) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_DAT2, (data >>16) & 0xff ) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_DAT3, (data >>24) & 0xff ) );
	wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_CMD, 0x01 ) );

	m_deppusb->deppWriteMany( wbDeppPair );
    }

    uint32_t WishboneController::readFast( uint8_t device, uint32_t address )
    {
    std::vector< std::pair<uint8_t, uint8_t > > wbDeppPair;

    wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_ADR3, device ) );
    wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_ADR0, address & 0xff ) );
    wbDeppPair.push_back( std::make_pair( WishboneDeppAddresses::WB_CMD, 0x02 ) );

    m_deppusb->deppWriteMany( wbDeppPair, false);

	uint32_t read_data = 0;
	read_data |= m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT0 );
	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT1 )) << 8;
 	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT2 )) << 16;
 	read_data |= ((uint32_t)m_deppusb->deppRead( WishboneDeppAddresses::WB_DAT3 )) << 24;
	LOG(Log::INF) << "- wishbone read fast: data=" << std::hex << read_data << std::dec;
	return read_data;
    }

    uint8_t WishboneController::getInterruptFlag()
    {
	// TODO: AND 0x04 is Piotr's hack
	return m_deppusb->deppRead( WishboneDeppAddresses::WB_INTV ) & 0x04;
    }

}
