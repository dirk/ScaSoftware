/*
 * DeppUsbHdlcBackend.cpp
 *
 *  Created on: Sep 21, 2016
 *      Author: pnikiel  // TODO: add all 3 of us here
 */

#include <stdexcept>
#include <DeppUsb/DeppUsbHdlcBackend.h>
#include <Sca/Frame.h>
#include <LogIt.h>


namespace DeppUsb
{

    // this is Wishbone device Id for Primary Elink Master
    const uint8_t ElinkMasterPrimaryDeviceId = 0x02;
    
    enum ELinkMasterMem
    {
	Control = 0x00,
	Status = 0x01,
	SendCmd = 0x02,
	SendData = 0x03,
	RecvCmd = 0x02,
	RecvData = 0x03,
	Enable = 0x06
    };

    enum ELinkMasterCommands
    {
	Send = 0x10,
	Connect = 0x1a,
	Reset = 0x1b
    };

    HdlcBackend::HdlcBackend(const std::string& specificAddress):
	m_specificAddress( specificAddress ),
	m_deppUsb( specificAddress ),
	m_wishbone( &m_deppUsb )
    {
	LOG(Log::INF) << "DeppUsb interface with specific address: " << specificAddress << " started up.";
	
	// read power enable
	unsigned int val = m_deppUsb.deppRead( 3 ); // ctrl reg
	LOG(Log::INF) << "CTRL register of board: " << val; 

	//	connect();
	//LOG(Log::INF) << "Elink connect sent successfully";
	//reset();
	//LOG(Log::INF) << "Elink reset sent successfully";
	//enable();
	//connect();
	LOG(Log::INF) << "Elink is enabled? " << isEnabled() ;
	m_recvThread = boost::thread( [this](){ recvThread(); } );
	usleep (1000000);

    }

    HdlcBackend::~HdlcBackend()
    {

    }

    void HdlcBackend::send(const Hdlc::Payload& request)
    {
	boost::lock_guard<boost::mutex> lock (m_atomizer);
	Sca::Frame frame (request);
	// 0x04 below is because length is always assumed as 4
	uint32_t commandWord = (0x04 << 24) | (frame.commandOrError() << 16) | (frame.transactionId() << 8) | frame.channelId();
	// TODO: add handling for data! for now data is ignored and length assumed as 4 bytes ...
	
	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::SendCmd, commandWord );
	uint32_t data = 0;
	auto requestData = request.data();
	for (unsigned int i=0; i< request.size()-4; i++)
	{
	    data = data << 8; 
	    data |= ((uint32_t)requestData[4+i]);
	}
	// additional shifts in case less than 4 bytes sent
	data <<= 8*(8-request.size());

	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::SendData, data );
	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Control, ELinkMasterCommands::Send );
	LOG(Log::INF) << "sent frame: " << frame.toString();
    }

    void HdlcBackend::connect ()
    {
	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Control, ELinkMasterCommands::Connect );
	uint32_t ack = m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Control );
	if (ack != 0x01)
	    throw std::runtime_error("when trying ELink connect: expected ack 0x01, obtained: TODO");
    }

    void HdlcBackend::reset ()
    {
	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Control, ELinkMasterCommands::Reset );
	uint32_t ack = m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Control );
	if (ack != 0x01)
	    throw std::runtime_error("when trying ELink reset: expected ack 0x01, obtained: TODO");
    }

    void HdlcBackend::enable ()
    {
	m_wishbone.write( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Enable, 1 );
    }

    bool HdlcBackend::isEnabled ()
    {
	return m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Enable) == 1;
    }

    void HdlcBackend::subscribeReceiver(ReceiveCallBack f)
    {
	m_receivers.push_back(f);
    }

    void HdlcBackend::recvThread()
    {
	LOG(Log::INF) << "inside recv-thread!";
	while (1)
	{
	    { // this is put as block to let the lock_guard be destroyed(=freed)
		boost::lock_guard<boost::mutex> lock (m_atomizer);
		uint8_t intv = m_wishbone.getInterruptFlag();
		if (intv != 0)
		{
		    // first: clean the int!
		    LOG(Log::INF) << "INTERRUPT!!!! *******(reading status to clean the flag) (iv=" << (unsigned int)intv << ")";
		    m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::Status );
		    // TODO!
	      
		    uint32_t recv_word = m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::RecvCmd);
		    uint32_t recv_data = m_wishbone.read( ElinkMasterPrimaryDeviceId, ELinkMasterMem::RecvData);
		    LOG(Log::INF) << std::hex << "recv word=" << recv_word << "," << "recv data=" << recv_data << std::dec;

		    //TODO: check this, might be broken
		    //Hdlc::Payload payload( { (recv_word>>8)&0xff, recv_word&0xff, (recv_word>>16)&0xff, (recv_word>>24)&0xff}, {} );
		    Sca::Frame payload ( recv_word&0xff, recv_word>>24, {(uint8_t)(recv_data>>24), (uint8_t)(recv_data>>16), (uint8_t)(recv_data>>8), (uint8_t)(recv_data)} );
		    payload.assignTransactionId ( (recv_word>>8) & 0xff );
		
		    std::for_each( m_receivers.begin(), m_receivers.end(), [&](ReceiveCallBack &cb){ cb(payload); } ); 
		}
	    }
	    usleep(10000);
	}
    }

} /* namespace DeppUsb */
