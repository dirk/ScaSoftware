# This script primarily serves to configure your SCA-SW dependencies for usage with
# TDAQ FELIX software (netio, felixbase, etc)

# You should source this file, not execute it!

# Authors: Paris, Piotr and Dirk


if [ $# -eq 0 ]; then
    FELIX_VERSION=03-08-00-13-g2f6b9b1
	#FELIX_VERSION=03-09-00-1-g5e27f1a (from CERN tests)
else
	FELIX_VERSION=$1
fi

PLATFORM=gcc62-opt
CVMFS_LCG=/cvmfs/sft.cern.ch/lcg/releases

#find RH release
rel=$(sed -e 's/\([^:]*:\)\{3\}//;s/:[a-z]*$//;s/:\([0-9]\)\+\.[0-9]*$/\1/' \
	/etc/system-release-cpe)
echo Found $rel.

# OS depending settings:
#decide if we are on SLC6 or CC7
case $rel in

	(centos*):
	echo "We're on CentOS..."
	export CXX=$CVMFS_LCG/gcc/6.2.0/x86_64-centos7/bin/c++
	export CC=$CVMFS_LCG/gcc/6.2.0/x86_64-centos7/bin/gcc
	export BOOST=$CVMFS_LCG/LCG_87/Boost/1.62.0/x86_64-centos7-gcc62-opt

	export FELIX=/FELIX/Software/felix-$FELIX_VERSION/x86_64-centos7-$PLATFORM/
	;;

	(*):
	echo "We guess we're on SLC(6) ..."
	export CXX=$CVMFS_LCG/gcc/6.2.0/x86_64-slc6/bin/c++
	export CC=$CVMFS_LCG/gcc/6.2.0/x86_64-slc6/bin/gcc
	export BOOST=$CVMFS_LCG/LCG_87/Boost/1.62.0/x86_64-slc6-gcc62-opt

	#export FELIX=/FELIX/Software/felix-$FELIX_VERSION/x86_64-slc6-$PLATFORM/
	export FELIX=/data/FELIX-soft/felix-$FELIX_VERSION/x86_64-slc6-$PLATFORM/
	;;

esac

# And generic definitions
#These are the same everywhere
export BOOST_LIBS="-lboost_regex-gcc62-mt-1_62 \
	-lboost_chrono-gcc62-mt-1_62 \
	-lboost_program_options-gcc62-mt-1_62 \
	-lboost_thread-gcc62-mt-1_62 \
	-lboost_system-gcc62-mt-1_62 \
	-lboost_filesystem-gcc62-mt-1_62"
#And these are defined via $BOOST
export BOOST_HEADERS=$BOOST/include/boost-1_62
export BOOST_LIB_DIRECTORIES=$BOOST/lib


if [ ! -d $FELIX ]; then
	echo $FELIX not found.
	echo " FELIX software not found in standard place. This will not work."
	echo " Please adjust \$FELIX variable or give s/w version as argument."
	fi

