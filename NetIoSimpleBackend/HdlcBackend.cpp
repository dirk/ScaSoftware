#include <LogIt.h>
#include <algorithm>
#include <boost/bind.hpp>

#include <NetIoSimpleBackend/HdlcBackend.h>
#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>

#include <iostream>

#include <felixbase/client.hpp>

// http://atlas-felix.cern.ch/netio/


namespace NetIoSimpleBackend
{

const unsigned int NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const unsigned int HDLC_FRAME_PAYLOAD_OFFSET = 2;   // HDLC payload offset in HDLC frame
const unsigned int HDLC_FRAME_TRAILER_SIZE = 2;   // HDLC payload offset in HDLC frame

/* The following constant relates to Henk B. method of achieving (relatively small - up to say 50 us) delays on the HDLC-encoded elink.
 * A good reading on Henk's method is in: https://its.cern.ch/jira/browse/FLX-581
 * The constant below defines how many zeros to put per microsecond of delay.
 * HDLC Elink throughput is 80Mbps, so 80 bits goes through each 1us.  80bits is 10 bytes that's why we insert 10 bytes to delay by 1 us.
 */
const unsigned int FLX_ZEROS_PER_MICROSECOND = 10;

HdlcBackend::HdlcBackend (const std::string& specificAddress):
        		m_originalSpecificAddress(specificAddress),
				m_specificAddress( specificAddress ),
				m_netioContext("posix"),
				m_netioRequestSocket( &m_netioContext ),
				m_netioReplySocket( &m_netioContext, boost::bind(&HdlcBackend::replyHandler, this, _1, _2) ),
				m_seqnr (7) // will be incremented before 1st use, and it is modulo 8
{

	LOG(Log::INF) << "Opening backend for hostname=" << m_specificAddress.felixHostname()
			<< ", port to SCA: " << m_specificAddress.portToSca()
			<< ", SCA elink: " << std::hex << m_specificAddress.elinkId();
	m_netioThread = boost::thread( [this](){ netioMainThread(); } );
	m_netioRequestSocket.connect(netio::endpoint( m_specificAddress.felixHostname(), m_specificAddress.portToSca()));
	LOG(Log::INF) << "Subscribing to elink: " << std::hex << (int)m_specificAddress.elinkId();
	m_netioReplySocket.subscribe( m_specificAddress.elinkId(), netio::endpoint( m_specificAddress.felixHostname(), m_specificAddress.portFromSca() ));



	usleep(1000000);
	sendHdlcControl( HDLC_CTRL_RESET );
	usleep(100000);
}

HdlcBackend::~HdlcBackend ()
{
	// TODO: disconnecting the interface or ...
}

std::string vectorAsHex (const std::vector<uint8_t> & v )
{
    std::stringstream ss;
    for (uint8_t octet : v)
    {
        ss.width(2);
        ss.fill('0');
        ss << std::hex << (unsigned int)octet << ' ';
    }
    return ss.str();
}

void HdlcBackend::send (const Hdlc::Payload &request )
{
    std::vector<Hdlc::Payload> requests ({ request });
    std::vector<unsigned int> times ({0});
    this->send( requests, times );
}

void HdlcBackend::send (
          const std::vector<Hdlc::Payload> &requests,
          const std::vector<unsigned int> times
      )
{

    if (requests.size() != times.size())
        throw_runtime_error_with_origin("requests vector has different size than times vector!");

    std::vector<unsigned char> netioFrame;
    netioFrame.reserve( 8192 );


    for (unsigned int i=0; i<requests.size(); ++i)
    {
        const Hdlc::Payload& request = requests[i];
        m_seqnr = (m_seqnr+1)%8;
        std::vector<uint8_t> hdlc_encoded_frame( HDLC_FRAME_PAYLOAD_OFFSET + request.size() + HDLC_FRAME_TRAILER_SIZE, 0 );
        felix::hdlc::encode_hdlc_msg_header( &hdlc_encoded_frame[0], /*HDLC address, always 0 for SCA*/0, m_seqnr);
        std::copy( request.cbegin(), request.cend(), &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET] );
        felix::hdlc::encode_hdlc_trailer(
                &hdlc_encoded_frame[ HDLC_FRAME_PAYLOAD_OFFSET + request.size()], /* where to put the trailer */
                &hdlc_encoded_frame[0], /* pointer to the beginning of HDLC frame*/
                request.size() + HDLC_FRAME_PAYLOAD_OFFSET);


        felix::base::ToFELIXHeader header;
        header.length = hdlc_encoded_frame.size();
        header.reserved = 0;
        header.elinkid = m_specificAddress.elinkId();
        uint8_t * const headerPtr = reinterpret_cast<uint8_t*>(&header);

        std::vector<uint8_t> thisChunk ( hdlc_encoded_frame.size() + sizeof(header), 0 );
        std::copy(
                headerPtr,
                headerPtr + sizeof(header),
                &thisChunk[0] );

        std::copy(
                hdlc_encoded_frame.begin(),
                hdlc_encoded_frame.end(),
                &thisChunk[sizeof header] );

        netioFrame.insert( netioFrame.end(), thisChunk.begin(), thisChunk.end() );

        if (times[i] > 0)
        {
            /* We will glue a block of zeros after the contents for a delay in Henk B. method */
            const unsigned int numBytes = FLX_ZEROS_PER_MICROSECOND * times[i];
            header.length = numBytes;
            netioFrame.insert( netioFrame.end(), headerPtr, headerPtr+sizeof header );
            netioFrame.insert( netioFrame.end(), numBytes, 0);
        }

        m_statistician.onFrameSent();
    }

    LOG(Log::TRC) << "sending netio request frame, group_size=" << requests.size() << ", frame_size=" << netioFrame.size();

    LOG(Log::TRC) << "contents: " << vectorAsHex(netioFrame);


    netio::message msg( &netioFrame[0], netioFrame.size() );

    m_netioRequestSocket.send( msg );

}

void HdlcBackend::netioMainThread(void)
{
	LOG(Log::INF) << "Inside netio thread for address: " << m_originalSpecificAddress;
	m_netioContext.event_loop()->run_forever();
}

void HdlcBackend::replyHandler( netio::endpoint&, netio::message& m )
{
	LOG(Log::INF) << "received reply";
	std::vector<uint8_t> netioFrame ( m.data_copy() );

	if (netioFrame.size() < 16)  // the smallest SCA reply (4 bytes) is 16 bytes of netio frame minimum - 8 for netio header and another 4 for HDLC header and trailer
	{
		LOG(Log::ERR) << "ignoring possible broken frame smaller than 16 bytes (len" << netioFrame.size() << ")";
		return;
	}


	// Cut the Address and the Control field from the HDLC frame | and the FCS at the end -- Paris
	uint8_t* hdlcPayloadPtr = &netioFrame[ NETIO_FRAME_HDLC_START_OFFSET + HDLC_FRAME_PAYLOAD_OFFSET ];

	Hdlc::Payload payload( hdlcPayloadPtr, netioFrame.size() - NETIO_FRAME_HDLC_START_OFFSET - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);
	std::for_each( m_receivers.begin(), m_receivers.end(), [&payload]( ReceiveCallBack& cb){ cb(payload); });
	m_statistician.onFrameReceived();
}

void HdlcBackend::sendHdlcControl (uint8_t hdlcControl)
{


	felix::base::ToFELIXHeader header;

	std::vector<unsigned char> netioFrame ( sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET + HDLC_FRAME_TRAILER_SIZE );

    header.length = netioFrame.size() - sizeof(header);
    header.reserved = 0;
    header.elinkid = m_specificAddress.elinkId();

    std::copy(
            reinterpret_cast<uint8_t*>(&header),
            reinterpret_cast<uint8_t*>(&header)+sizeof header,
            netioFrame.begin() );
	felix::hdlc::encode_hdlc_ctrl_header( &netioFrame[ sizeof header ], 0, hdlcControl );
	felix::hdlc::encode_hdlc_trailer(
	        &netioFrame[0] + sizeof(header) + HDLC_FRAME_PAYLOAD_OFFSET, /* where to put the trailer */
	        &netioFrame[0] + sizeof(header),
	        HDLC_FRAME_PAYLOAD_OFFSET);
	netio::message message( &netioFrame[0], netioFrame.size() );
	LOG(Log::INF) << "Sending control: " << std::hex << (unsigned int)hdlcControl;
	LOG(Log::TRC) << "Size: " << netioFrame.size() << " contents: " << vectorAsHex(netioFrame);
	m_netioRequestSocket.send(message);
}

}
