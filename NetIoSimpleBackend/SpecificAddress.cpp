/*
 * SpecificAddress.cpp
 *
 *  Created on: Mar 15, 2017
 *      Author: pnikiel
 */

#include <sstream>

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>
#include <ScaCommon/except.h>

#include <LogIt.h>

namespace NetIoSimpleBackend
{

SpecificAddress::SpecificAddress (const std::string& stringAddress):
		m_elinkId(0) // is parsed in the body
{
	// Piotr: you can use regex101.com to help yourself designing the regex
	boost::regex directAddressRegex( "^direct\\/"
			"(?<address>[a-zA-Z0-9][a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)\\/"
			"(?<port_to_sca>\\d+)\\/"
			"(?<port_from_sca>\\d+)\\/"
			"(?<elink_sca>(?:[0-9a-zA-Z])+)$" 
			);
	boost::smatch matchResults;
	bool matched = boost::regex_match( stringAddress, matchResults, directAddressRegex );
	if (!matched)
	{
		THROW_WITH_ORIGIN(std::runtime_error,
				"supplied specific address didn't match the regex for direct address. Given string was: '"+
				stringAddress+"' Here is an example: 'direct/pcatlnswfelix01.cern.ch/12340/12345/BF'");
	}
	m_felixHostname = matchResults["address"];
	m_portToSca = boost::lexical_cast<unsigned int>(matchResults["port_to_sca"]);
	m_portFromSca = boost::lexical_cast<unsigned int>(matchResults["port_from_sca"]);
	// elinks should be given in hex, that's why lexical_cast can't be used
	std::stringstream ss;
	ss.str(matchResults["elink_sca"]);
	ss >> std::hex >> m_elinkId;
	if (ss.fail())
		THROW_WITH_ORIGIN(std::runtime_error,"Didn't manage to read hex number from: "+ss.str());

}



}



