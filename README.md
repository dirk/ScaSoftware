Quick startup guide
-------------------

For standalone SCA SW (which will let you run some demonstrators to talk to SCA's peripherals), just initialise
the environment by 

source setup_paths.sh [03-08-01] # optionally specifying version 03-08-01

and then

./build_standalone.sh

Note that:
1. By default, the software builds with netio and other parts of the TDAQ FELIX Software. 
You can remove netio dependency by changing, in  build_standalone, HAVE_NETIO to 0.

2. The easiest way to get netio and TDAQ FELIX is to get the distro from:
https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/3.x/

The adviced packages are:
- for CC7:
 https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/3.x/felix-03-04-02-x86_64-centos7-gcc49-opt.tar.gz
- for SLC6:
 https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/3.x/felix-03-04-02-x86_64-slc6-gcc49-opt.tar.gz

3. Please note that you should use exactly same compiler as the one used for TDAQ FELIX chain. 
Use gcc49 from LCG84 distribution.


Prerequisites
-------------

If things don't go smoothly, check that you installed the following dependencies:
- tbb-devel

