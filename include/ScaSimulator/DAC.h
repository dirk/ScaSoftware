/*
 * DAC.h
 *
 *  Created on: November 7, 2016
 *      Author: aikoulou, based on pnikiel
 */

#ifndef SCASIMULATOR_DAC_H_
#define SCASIMULATOR_DAC_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class DAC: public ScaChannel
{
public:

	DAC( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~DAC ();

	virtual void onReceive( const Sca::Request &request );

private:
	uint8_t m_DAC_A   ;// read/write: this is the register for DAC channel A
	uint8_t m_DAC_B   ;// read/write: this is the register for DAC channel B
	uint8_t m_DAC_C   ;// read/write: this is the register for DAC channel C
	uint8_t m_DAC_D   ;// read/write: this is the register for DAC channel D
	bool isDacReadCommand(uint32_t);
	bool isDacWriteCommand(uint32_t);
	void sendReadReply(const Sca::Request& request,uint32_t);
	void sendWriteReply(const Sca::Request& request,uint32_t, uint8_t);
	uint8_t getRegisterFromCommand(uint32_t);
	void setRegisterFromCommand(uint32_t,uint8_t);
/*
	void sendReadReply(const Sca::Frame& request,uint32_t);
	void sendWriteReply(const Sca::Frame& request,uint32_t,uint32_t);
	uint32_t getRegisterFromCommand(uint32_t);
	void setRegisterFromCommand(uint32_t,uint32_t);
*/
};

}

#endif /* SCASIMULATOR_DAC_H_ */
