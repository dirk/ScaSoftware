/*
 * SPI.h
 *
 *  Created on: Nov 3, 2016
 *      Author: pmoschov
 */


#ifndef SCASIMULATOR_SPI_H_
#define SCASIMULATOR_SPI_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class SPI: public ScaChannel
{
public:

	SPI( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~SPI ();

	virtual void onReceive( const Sca::Request &request );
	uint16_t m_divider = 0;
};

}

#endif /* SCASIMULATOR_SPI_H_ */
