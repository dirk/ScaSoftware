/*
 * except.h
 *
 *  Created on: May 4, 2016
 *      Author: pnikiel
 */

#ifndef COMMON_EXCEPT_H_
#define COMMON_EXCEPT_H_

#include <stdexcept>
#include <boost/lexical_cast.hpp>

// note: the following macro should be replaced with more generic THROW_WITH_ORIGIN
#define throw_runtime_error_with_origin(MSG) throw std::runtime_error(std::string("At ")+__PRETTY_FUNCTION__+" "+MSG)

#define THROW_WITH_ORIGIN(WHAT,MSG) throw WHAT (std::string("At ")+__FILE__+":"+boost::lexical_cast<std::string>(__LINE__)+" in "+__PRETTY_FUNCTION__+" "+MSG)



#endif /* COMMON_EXCEPT_H_ */
