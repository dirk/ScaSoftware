/**

TODO data, author, purpose

*/

#ifndef __WISHBONECONTROLLER_H__
#define __WISHBONECONTROLLER_H__

#include <DeppUsb/DeppUsb.h>
#include <vector>
#include <utility>

namespace DeppUsb
{



    class WishboneController
    {

    public:
	WishboneController( DeppUsb * deppusb_instance, bool fast=true );

	void write( uint8_t device, uint32_t address, uint32_t data);
	uint32_t read( uint8_t device, uint32_t address); // TODO: should we add nbytes?
	void writeFast( uint8_t device, uint32_t address, uint32_t data);
	uint32_t readFast( uint8_t device, uint32_t address);
	uint8_t getInterruptFlag();

    private:
	DeppUsb *m_deppusb;
	bool m_fast;

    };

}

#endif // __WISHBONECONTROLLER_H__
