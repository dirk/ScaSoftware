/*
 * DeppUsb.h
 *
 *  Created on: Sep 20, 2016
 *      Author: pnikiel
 */

#ifndef DEPPUSB_DEPPUSB_H_
#define DEPPUSB_DEPPUSB_H_

#include <stdint.h>
#include <string>
#include <vector>
#include <utility>

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>

#include <dpcdecl.h>


namespace DeppUsb
{

class DeppUsb {
public:
	DeppUsb(const std::string& deviceName, bool verbose=false);
	virtual ~DeppUsb();

	void deppWrite( uint8_t address, uint8_t data, bool overlap=false);
	uint8_t deppRead( uint8_t address, bool overlap=false);
	void deppWriteMany( const std::vector< std::pair<uint8_t, uint8_t > > deppDataPair, bool overlap=false);

private:
	HIF m_hif;
	void throwLastError(std::string userMessage="Dmgr/Depp error: ");
	bool findDvc(char * szDev, DVC * pdvc);
	boost::mutex m_single_access_mutex;
	

};


} /* namespace DeppUsb */

#endif /* DEPPUSB_DEPPUSB_H_ */
