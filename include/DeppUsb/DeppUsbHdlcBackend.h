/*
 * DeppUsbHdlcBackend.h
 *
 *  Created on: Sep 21, 2016
 *      Author: pnikiel
 */

#ifndef DEPPUSBHDLCBACKEND_H_
#define DEPPUSBHDLCBACKEND_H_

#include <boost/thread.hpp>
#include <Hdlc/Backend.h>
#include <DeppUsb/DeppUsb.h>
#include <DeppUsb/WishboneController.h>

namespace DeppUsb
{

class HdlcBackend: public Hdlc::Backend
{
public:
	HdlcBackend(const std::string& specificAddress);
	virtual ~HdlcBackend();

	  virtual void send (const Hdlc::Payload &request );
	  virtual void subscribeReceiver ( ReceiveCallBack f );
	  virtual std::string getAddress ( ) const { return m_specificAddress; }

private:
	  // TODO : connect probably to move to separate ELinkMaster class?
	  void connect(); // establish ELink connection
	  // TODO: same for reset?
	  void reset();
	  void enable();
	  bool isEnabled();

	  void recvThread(); // infinite loop scanning for wishbone interrupt

	  std::string m_specificAddress;
	  DeppUsb m_deppUsb;
	  WishboneController m_wishbone;
	  boost::thread m_recvThread;
	  std::list<ReceiveCallBack> m_receivers;
	  boost::mutex m_atomizer;
};

} /* namespace DeppUsb */

#endif /* DEPPUSBHDLCBACKEND_H_ */
