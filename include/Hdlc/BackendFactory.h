
#ifndef BACKENDFACTORY_H
#define BACKENDFACTORY_H

#include <Hdlc/Backend.h>

#include <boost/thread/mutex.hpp>

#include <string>
#include <map>

namespace Hdlc {

class BackendFactory
{
public:



  /**
   * @return Hdlc::Backend *
   * @param  scaAddress
   */
  static Backend * getBackend (const std::string& scaAddress ) ;
private:
  static boost::mutex m_accessLock;
  static std::map<std::string, Backend*> m_openedBackends;
};
} // end of package namespace

#endif // BACKENDFACTORY_H
