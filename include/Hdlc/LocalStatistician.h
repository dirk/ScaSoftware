/*
 * LocalStatistician.h
 *
 *  Created on: 1 Aug 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_HDLC_LOCALSTATISTICIAN_H_
#define INCLUDE_HDLC_LOCALSTATISTICIAN_H_

#include <atomic>
#include <Hdlc/Backend.h>

#include <chrono>

namespace Hdlc
{

class LocalStatistician
{
public:
    LocalStatistician();

    BackendStatistics toBackendStatistics () const ;

    void onFrameSent () { m_numberFramesSent.fetch_add(1); }
    void onFrameReceived ();

private:

    std::atomic_uint_fast64_t m_numberFramesSent;
    std::atomic_uint_fast64_t m_numberFramesReceived;
    std::atomic_uint_fast64_t m_lastReplyTimeStamp;



};


}


#endif /* INCLUDE_HDLC_LOCALSTATISTICIAN_H_ */
