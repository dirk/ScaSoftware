
#ifndef PAYLOAD_H
#define PAYLOAD_H

#include <vector>
#include <stdint.h>
#include <string>
#include <initializer_list>
#include <ScaCommon/except.h>

namespace Hdlc {

    class Payload
    {
    public:

	Payload (const std::vector<uint8_t>& data);
	Payload (const uint8_t* data, size_t length);

	//! Handy to initialize i.e. SCA Frames as they are made from two parts (const-size and variable-size one)
	Payload( std::initializer_list<uint8_t> data1, std::initializer_list<uint8_t> data2);

	/*
	 * Paris: Hybrid Payload. First part is an initializer_list while the second is the varied data
	 * passed to the different SCA interface. Length refers to the size of the interface payload.
	 */
	Payload (std::initializer_list<uint8_t> data1, uint8_t* data2, uint8_t length);

	/**
	 * @return unsigned int
	 */
	unsigned int size ( ) const { return m_dataSize; }



	std::string toString() const;

	uint8_t operator[](unsigned int index) const { if ( index >= m_dataSize ) THROW_WITH_ORIGIN(std::runtime_error,"out-of-bounds"); return m_data[ index ]; }

	//! STL type const-iterators
	const uint8_t* cbegin() const { return m_data; }
	const uint8_t* cend() const { return m_data + m_dataSize; }


    protected:

	uint8_t m_data [8];
	uint8_t m_dataSize;


    };
} // end of package namespace

#endif // PAYLOAD_H
