/*
 * ScaSwVeryHipEventTypes.h
 *
 *  Created on: 11 Dec 2017
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_SCASWVERYHIPEVENTTYPES_H_
#define INCLUDE_SCA_SCASWVERYHIPEVENTTYPES_H_

#ifdef WITH_EVENT_RECORDER

#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::record_event;

class SendEvent: public VeryHipEvents::Event
{
public:

    SendEvent (uint8_t trIdFrom, uint8_t trIdTo) :
        VeryHipEvents::Event(),
        m_trIdFrom(trIdFrom),
        m_trIdTo(trIdTo)
    {
    }

    virtual std::string toXmlElement() const
    {
        return "<send "+basicAttributes()+" trIdFrom='"+boost::lexical_cast<std::string>((unsigned int)m_trIdFrom)+"' trIdTo='"+boost::lexical_cast<std::string>((unsigned int)m_trIdTo)+"' />";
    }
private:
    uint8_t m_trIdFrom;
    uint8_t m_trIdTo;
};

class ReceiveEvent: public VeryHipEvents::Event
{
public:

    ReceiveEvent (uint8_t trId) :
        VeryHipEvents::Event(),
        m_trId (trId)
    {
    }

    virtual std::string toXmlElement() const
    {
        return "<receive "+basicAttributes()+" trId='"+boost::lexical_cast<std::string>((unsigned int)m_trId)+"' />";
    }
private:
    uint8_t m_trId;
};

#endif // STANDALONE_WITH_EVENT_RECORDER

#endif /* INCLUDE_SCA_SCASWVERYHIPEVENTTYPES_H_ */
