
#ifndef FRAMES_H
#define FRAMES_H

#include <string>
#include <vector>
#include <Hdlc/Payload.h>

namespace Sca {

class Frames
{
public:



  /**
   * @return Hdlc::Payload
   * @param  channelId
   * @param  command
   * @param  data
   */
  static Hdlc::Payload makeRequestFrame (unsigned char channelId, unsigned char command, std::vector<uint8_t>& data );



};
} // end of package namespace

#endif // FRAMES_H
