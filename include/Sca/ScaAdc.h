/*
 * Adc.h
 *
 *  Created on: 29 Mar 2018
 *      Author: pnikiel
 */

#ifndef INCLUDE_SCA_SCAADC_H_
#define INCLUDE_SCA_SCAADC_H_

namespace Sca
{

class Adc
{
public:
  Adc (SynchronousService& ss): m_synchronousService(ss) {}
  virtual ~Adc () {}

  // convert channel is set input line + go.
  // it might speed-up your conversion when the same channel is converted
  // by not sending a setInputLine request
  virtual uint16_t convertChannel (uint8_t channelId) = 0;

  virtual uint16_t go () = 0;

  virtual void setInputLine ( unsigned int inputLine ) = 0;
  virtual unsigned int getInputLine () = 0;

  virtual void setCurrentSourceLine ( int line ) = 0;
  virtual int getCurrentSourceLine () = 0;

  virtual void setGainCalibration(uint32_t x) = 0;
  virtual uint32_t getGainCalibration() = 0;

protected:
  SynchronousService& m_synchronousService;
};

class AdcV1: public Adc
{
public:
    AdcV1 (SynchronousService& ss): Adc(ss) {}

    virtual uint16_t convertChannel (uint8_t channelId);
    virtual uint16_t go ();

    virtual void setInputLine ( unsigned int inputLine );
    virtual unsigned int getInputLine ();

    virtual void setCurrentSourceLine ( int line );
    virtual int getCurrentSourceLine ();

    virtual void setGainCalibration(uint32_t x);
    virtual uint32_t getGainCalibration();
};

class AdcV2: public Adc
{
public:
    AdcV2 (SynchronousService& ss): Adc(ss) {}

    virtual uint16_t convertChannel (uint8_t channelId);
    virtual uint16_t go ();

    virtual void setInputLine ( unsigned int inputLine );
    virtual unsigned int getInputLine ();

    virtual void setCurrentSourceLine ( int line );
    virtual int getCurrentSourceLine ();

    virtual void setGainCalibration(uint32_t x);
    virtual uint32_t getGainCalibration();
};

}

#endif /* INCLUDE_SCA_SCAADC_H_ */
