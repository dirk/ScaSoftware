/*
 * Sca.h
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel,aikoulou, pmoschov
 */

#ifndef SCA_SCA_H_
#define SCA_SCA_H_

#include <string>
#include <memory> // for unique_ptr
#include <Sca/SynchronousService.h>

#include <Sca/Defs.h>
#include <Hdlc/Backend.h>  // to access backend statistics

#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>

#include <Sca/ScaAdc.h>

namespace Hdlc
{
class Backend;
}

namespace Sca
{
	// move it to impl specific files? no reason the user would need it
	void throwIfScaReplyError(const Reply& reply);

    class Sca
    {
    public:



	class Spi
	{
	public:
		Spi ( SynchronousService& ss): m_synchronousService( ss ), m_scav1(false) {}

		void configureBus ( unsigned int transmissionBaudRate, uint8_t transmissionBitLength );
		void writeSlave ( int selectedSlave, std::vector<uint8_t> &payload );

		void setSelectedSlave ( int slaveId );
		void resetSelectedSlave ();
		uint8_t getSelectedSlave ();

		void setTransmissionBaudRate ( unsigned int transmissionBaudRate );
		float getTransmissionBaudRate ();

		/* allowed range 1-128 bits */
		void setTransmisionBitLength ( unsigned int transmissionBitLength );
		uint8_t getTransmisionBitLength ();

		bool getInvSclk ();
		void setInvSclk ( bool setSclkInv );

		bool getBusyFlag ();
		void setGoFlag ( bool setGoFlag );

		bool getRxEdge ();
		void setRxEdge ( bool setRxEdge );

		bool getTxEdge ();
		void setTxEdge ( bool setTxEdge );

		bool getLsbToMsb ();
		void setLsbToMsb ( bool setMsbToLsb );

		bool isInterruptEnabled ();
		void setInterruptEnable ( bool setInterruptEnable );

		bool getSsMode ();
		void setSsMode ( bool setSsMode );

		bool getBusy ();
		void manualSpiGo ( bool manualSpiGo );

		std::vector<uint32_t> getSlaveReply ();

		// TODO: remove this when we move to SCA-V2 only
		void setScaV1 (bool v1) { m_scav1=v1; };

		//TODO: This can be done in a smarter way
		/* 00, 01, 10, 11 */
		void setSpiMode ( uint8_t spiMode );
		int getSpiMode ();

		// Initial values of an SCA when powered on
		float m_baudRate = 20000000.;
		unsigned int m_transmissionBitLength = 96;

		bool m_setSclkInv = 0;
		bool m_setGoFlag = 0;
		bool m_setRxEdge = 0;
		bool m_setTxEdge = 0;
		bool m_setMsbToLsb = 0;
		bool m_setInterruptEnable = 1;
		bool m_setSsMode = 1;

		uint8_t m_spiMode = 0;

		//TODO: WIP: Those should be private. Changed due to integration week debugging of VMM
		uint16_t getControlRegister();
		void writeSpiSlaveChunk ( std::vector<uint8_t>::iterator &dataIter );

	private:

		unsigned int getSpiIterations ( std::vector<uint8_t> &payload );


		SynchronousService& m_synchronousService;
		bool m_scav1;

	};

	class Gpio
	{
	private:
		//LOW-LEVEL
		uint32_t getReadCommandFromRegister(uint32_t);
		uint32_t getWriteCommandFromRegister(uint32_t);
		void sendWriteDataToRegister(uint32_t,uint32_t);
		uint32_t sendReadRegister(uint32_t);

	public:
		Gpio (SynchronousService& ss): m_synchronousService(ss) {}

		//USER LEVEL (using low-level functions)
		//for registers
		uint32_t getRegister(uint32_t);
		void setRegister(uint32_t,uint32_t);

		bool getRegisterBitValue(uint32_t,int);
		void setRegisterBitToValue(uint32_t, int, bool);

		std::vector<bool> getRegisterRangeValue(uint32_t,int,int);
		void setRegisterRangeToValue(uint32_t, int,int,bool);


	private:
		SynchronousService& m_synchronousService;
		boost::mutex m_single_access_mutex_GPIO;
	};//GPIO

	class Dac
	{
	public:
		Dac (SynchronousService& ss): m_synchronousService(ss) {}

		void writeRegister(::Sca::Constants::Dac,uint8_t);
		uint8_t readRegister(::Sca::Constants::Dac);
		float getVoltageEstimateAtRegister(::Sca::Constants::Dac);
		float getVoltageStepEstimate();
		void incrementChannelByStep(::Sca::Constants::Dac);
		void decrementChannelByStep(::Sca::Constants::Dac);
		void setVoltageOnChannel(::Sca::Constants::Dac,float);

	private:
		SynchronousService& m_synchronousService;
		boost::mutex m_single_access_mutex_DAC;
		::Sca::Constants::Commands getWriteCommandFromRegister(::Sca::Constants::Dac);
		::Sca::Constants::Commands getReadCommandFromRegister(::Sca::Constants::Dac);
	};//DAC

	class Jtag
	{
	public:
		Jtag (SynchronousService& ss): m_synchronousService(ss) {}


		//configuration commands
		void setTransmissionLength_Bits(int);
		void setRxEdge(::Sca::Constants::Jtag);
		void setTxEdge(::Sca::Constants::Jtag);
		void setBitTransmissionOrder(::Sca::Constants::Jtag);
		void setTckIdleLevel(::Sca::Constants::Jtag);
		void setFrequencyDivider(uint16_t);
		void setFrequency_MHz(float);

		int getTransmissionLength_Bits();
		::Sca::Constants::Jtag getRxEdge();
		::Sca::Constants::Jtag getTxEdge();
		::Sca::Constants::Jtag getBitTransmissionOrder();
		::Sca::Constants::Jtag getTckIdleLevel();
		uint16_t getFrequencyDivider();
		float getFrequency_MHz();

		//transmission commands
		void sendByteVector(std::vector<uint8_t>);
		void sendFile(std::string);
		void programFPGAwithFile(std::string);
		void sendUpto8Bits_TMS_Length(uint8_t,bool,uint);
		void clockOutTMSForTime_ms(bool,uint);
		void getFpgaId();
		void getFpgaFuseDna();
		void getFpgaXscDna();
		void scanChain();

		//reset
		void reset();



	public:
		SynchronousService& m_synchronousService;

		boost::mutex m_single_access_mutex_JTAG;

		Request makeTDO(int, uint32_t);
		Request makeTMS(int, uint32_t);
		Request makeGO();
		Request makeTDI(int);


		void JTAG_GO();
		void writeTDO(int,uint32_t);
		void writeTMS(int,uint32_t);
		uint32_t readTDI(int);
		uint32_t readTMS(int);
		void writeCTRL(uint16_t);
		uint16_t readCTRL();

		void clockOutSingleTMS(bool); //don't care about TDO data
		void clockOutTMS_times(bool,int); //don't care about TDO data

	};//JTAG

	class I2c
	{
	public:
		I2c ( SynchronousService& ss ): m_synchronousService( ss ) {}

		uint8_t getControlRegister ( uint8_t i2cMaster );
		void setControlRegister ( uint8_t i2cMaster, uint8_t ctrlReg );
		void setFrequency ( uint8_t i2cMaster, uint16_t frequency );
		void setTransmissionByteLength ( uint8_t i2cMaster, uint8_t transmissionByteLength );
		void setSclMode ( uint8_t i2cMaster, bool sclMode );

		struct statusRegister
		{
			bool successfulTransaction, sdaLineLevelError, invalidCommandSent, lastOperationNotAcknowledged;
		};
		uint8_t getStatusRegister ( uint8_t i2cMaster );
		statusRegister getStatus( uint8_t i2cMaster, uint8_t statusReg );

		void setDataRegister ( uint8_t i2cMaster, std::vector<uint8_t> &data );
		std::vector<uint8_t> getDataRegister ( uint8_t i2cMaster );

		std::vector<uint8_t> readSingleByte7bit ( uint8_t i2cMaster, uint8_t address );
		uint8_t writeSingleByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t data );
		std::vector<uint8_t> readMultiByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t nbytes );
		uint8_t writeMultiByte7bit ( uint8_t i2cMaster, uint8_t address, std::vector<uint8_t> &data );

		std::vector<uint8_t> readSingleByte10bit ( uint8_t i2cMaster, uint16_t address );
		uint8_t writeSingleByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t data );
		std::vector<uint8_t> readMultiByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t nbytes );
		uint8_t writeMultiByte10bit ( uint8_t i2cMaster, uint16_t address, std::vector<uint8_t> &data );

		uint8_t write ( uint8_t i2cMaster, bool addressingMode, uint16_t address, std::vector<uint8_t> &data );
		std::vector<uint8_t> read ( uint8_t i2cMaster, bool addressingMode, uint16_t address, uint8_t nbytes );

		// Initial values of an SCA at power up. New values are cached here.
		uint8_t m_frequency = ::Sca::Constants::I2c::I2C_FREQ_100KHz;
		uint8_t m_nbyte = ::Sca::Constants::I2c::I2C_NBYTE_2;
		bool m_sclmode = ::Sca::Constants::I2c::I2C_SCLMODE_OPEN_DRAIN;
		uint8_t m_lasti2cMaster = 0;

	private:
		std::initializer_list<uint8_t>* getPayloads( std::initializer_list<uint8_t> &data );
		SynchronousService& m_synchronousService;

	};

	Sca( const std::string& address );
	virtual ~Sca();
	Adc& adc() { return *m_adc.get(); }
	Gpio& gpio() { return m_gpio; }
	Spi& spi() { return m_spi; }
	I2c& i2c() {return m_i2c;}
	Dac& dac() {return m_dac;}
	Jtag& jtag() {return m_jtag;}

	uint8_t getNodeControlCommand ( int scaChannel, bool forWrite );

	void setChannelEnabled ( int scaChannel, bool enabled );
	bool getChannelEnabled ( int scaChannel );

	// this one really invokes read transaction
	uint32_t readChipId ();

	// this one just reads the cached value read at start-up. note sca id rather don't change ;-)
	uint32_t getChipId () const { return m_cachedScaId; }

	Hdlc::BackendStatistics getBackendStatistics () const ;

	SynchronousService& synchronousService() { return m_synchronousService; }

    private:
	Hdlc::Backend * m_backend;
	SynchronousService m_synchronousService;
	std::unique_ptr<Adc> m_adc;
	Spi m_spi;
	Gpio m_gpio;
	Dac m_dac;
	Jtag m_jtag;
	I2c m_i2c;

	uint32_t m_cachedScaId;

    };

} /* namespace Sca */

#endif /* SCA_SCA_H_ */
