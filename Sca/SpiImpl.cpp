/*
 * SpiImpl.cpp
 *
 *  Created on: May 11, 2016
 *      Author: pmoschov
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>

using boost::lexical_cast;


namespace Sca
{

void Sca::Spi::configureBus( unsigned int transmissionBaudRate, uint8_t transmissionBitLength )
{

	setTransmissionBaudRate ( transmissionBaudRate );

	setTransmisionBitLength ( transmissionBitLength );

	if (m_scav1)
		setInterruptEnable(true);

}

void Sca::Spi::writeSlave ( int selectedSlave, std::vector<uint8_t> &payload )
{

	int spiIter = getSpiIterations( payload );
	LOG(Log::INF) << "Writing to slave: " << selectedSlave ;

	setSelectedSlave( selectedSlave );

	std::vector<uint8_t>::iterator dataIter;

	for ( int i = 0; i < spiIter; i++ )
	{

		dataIter = payload.begin() + (m_transmissionBitLength / 8) * i;
		LOG(Log::INF) << "Iteration: " << spiIter ;
		writeSpiSlaveChunk ( dataIter );

	}

	resetSelectedSlave();

}

void Sca::Spi::setSelectedSlave ( int slaveId )
{
	if (slaveId >= 8)
	    throw std::out_of_range("setSelectedSlave: argument out of range: "+slaveId );
	// TODO: Assuming only 1 SS can be active each time
	uint8_t slaveIdMask = 0x00;
	slaveIdMask |= 1 << slaveId;

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_SS,
	    {0, 0, slaveIdMask, 0} );
	m_synchronousService.sendAndWaitReply( request, 1000 );
}

void Sca::Spi::resetSelectedSlave ()
{

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_SS,
	    {0, 0, 0, 0} );
	m_synchronousService.sendAndWaitReply( request, 1000 );
}

uint8_t Sca::Spi::getSelectedSlave ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_SS,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );

	return reply[6];
}

std::vector<uint32_t> Sca::Spi::getSlaveReply ()
{

// TODO: Verify that MISO is using the same transmission length as the MOSI

Request request1(
	::Sca::Constants::ChannelIds::SPI,
	::Sca::Constants::Commands::SPI_R_MISO0,
	{} );
Reply reply1 = m_synchronousService.sendAndWaitReply( request1, 1000 );

std::vector<uint32_t> replies;
replies.push_back(reply1[7] << 24 | reply1[6] << 16 | reply1[5] << 8 | reply1[4]);

if (m_transmissionBitLength > 32)
{
    Request request2(
    	::Sca::Constants::ChannelIds::SPI,
    	::Sca::Constants::Commands::SPI_R_MISO1,
    	{} );
    Reply reply2 = m_synchronousService.sendAndWaitReply( request2, 1000 );
    replies.push_back(reply2[7] << 24 | reply2[6] << 16 | reply2[5] << 8 | reply2[4]);

    if (m_transmissionBitLength > 64)
    {
        Request request3(
        	::Sca::Constants::ChannelIds::SPI,
        	::Sca::Constants::Commands::SPI_R_MISO2,
        	{} );
        Reply reply3 = m_synchronousService.sendAndWaitReply( request3, 1000 );
        replies.push_back(reply3[7] << 24 | reply3[6] << 16 | reply3[5] << 8 | reply3[4]);

        if (m_transmissionBitLength > 96)
        {
            Request request4(
            	::Sca::Constants::ChannelIds::SPI,
            	::Sca::Constants::Commands::SPI_R_MISO3,
            	{} );
            Reply reply4 = m_synchronousService.sendAndWaitReply( request4, 1000 );
            replies.push_back(reply4[7] << 24 | reply4[6] << 16 | reply4[5] << 8 | reply4[4]);
        }
    }
}

return replies;
}

float Sca::Spi::getTransmissionBaudRate ()
{

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_FREQ,
	    {} );
	request.setLength(1);
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	int divider = reply[7] << 8 | reply[6] ;

	m_baudRate = 2. / (divider + 1) * 10000000;

	return m_baudRate;
}

void Sca::Spi::setTransmissionBaudRate ( unsigned int transmissionBaudRate )
{

	if ( transmissionBaudRate < 305 || transmissionBaudRate > 20000000 )
	    throw std::runtime_error("setTransmissionBaudRate: argument out of range: "+transmissionBaudRate );
	auto divider = (2 * 10000000 / transmissionBaudRate) - 1;
	if ( divider > 65535 ) divider = 65535;
	m_baudRate = 2. / ((divider + 1) * 10000000);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_FREQ,
	    {0, 0, (uint8_t) divider, (uint8_t) (divider >> 8)} );

	m_synchronousService.sendAndWaitReply( request, 1000 );

}

uint8_t Sca::Spi::getTransmisionBitLength ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	uint8_t length = reply[6] & 0x7f;
	// Value 0 represents 128 bits
	if ( !length )
		length = 128;

	return length;
}

void Sca::Spi::setTransmisionBitLength ( unsigned int transmissionBitLength )
{

	if ( transmissionBitLength > 128 || transmissionBitLength < 1 )
	    throw std::out_of_range( "setTransmisionBitLength: argument out of range: " + transmissionBitLength );

	m_transmissionBitLength = transmissionBitLength;
	if ( transmissionBitLength == 128 )
		transmissionBitLength = 0;

	uint16_t currentReg = getControlRegister();
	uint16_t updatedCtrlReg = ((uint8_t)transmissionBitLength << 8) | ( currentReg & 0x80FF );

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getInvSclk ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool isSclkInverted = reply[6] & 0x80;

	return isSclkInverted;
}

void Sca::Spi::setInvSclk ( bool setSclkInv )
{

	uint16_t currentReg = getControlRegister ();
	m_setSclkInv = setSclkInv;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setSclkInv ^ updatedCtrlReg) & (1 << 15);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getBusy ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool isBusy = reply[7] & 0x01;

	return isBusy;
}

void Sca::Spi::manualSpiGo ( bool manualSpiGo )
{

	uint16_t currentReg = getControlRegister ();
	m_setSsMode = manualSpiGo;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-manualSpiGo ^ updatedCtrlReg) & (1 << 4);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getBusyFlag ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool isBusy = reply[7] & 0x01;

	return isBusy;
}

void Sca::Spi::setGoFlag ( bool setGoFlag )
{

	uint16_t currentReg = getControlRegister ();
	m_setGoFlag = setGoFlag;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setGoFlag ^ updatedCtrlReg) & (1 << 0);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getRxEdge ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool sampleAtFallingEdge = reply[7] & 0x02;
	// '0': sample at the rising edge / '1': sample at the rising edge
	return sampleAtFallingEdge;
}

void Sca::Spi::setRxEdge ( bool setRxEdge )
{

	uint16_t currentReg = getControlRegister ();
	m_setRxEdge = setRxEdge;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setRxEdge ^ updatedCtrlReg) & (1 << 1);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getTxEdge ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool sampleAtFallingEdge = reply[7] & 0x04;
	// '0': sample at the rising edge / '1': sample at the rising edge
	return sampleAtFallingEdge;
}

void Sca::Spi::setTxEdge ( bool setTxEdge )
{

	uint16_t currentReg = getControlRegister ();
	m_setTxEdge = setTxEdge;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setTxEdge ^ updatedCtrlReg) & (1 << 2);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getLsbToMsb ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool lsbToMsb = reply[7] & 0x08;
	// '0': send from MSB to LSB / '1': send from LSB to MSB
	return lsbToMsb;
}

void Sca::Spi::setLsbToMsb ( bool setMsbToLsb )
{

	uint16_t currentReg = getControlRegister ();
	m_setMsbToLsb = setMsbToLsb;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setMsbToLsb ^ updatedCtrlReg) & (1 << 3);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::isInterruptEnabled ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool isInterruptEnabled = reply[7] & 0x10;

	return isInterruptEnabled;
}

void Sca::Spi::setInterruptEnable ( bool setInterruptEnable )
{

	uint16_t currentReg = getControlRegister ();
	m_setInterruptEnable = setInterruptEnable;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setInterruptEnable ^ updatedCtrlReg) & (1 << 4);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

bool Sca::Spi::getSsMode ()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	bool ssMode = reply[7] & 0x20;
	// '0': manual mode / '1': automatic mode
	return ssMode;
}

void Sca::Spi::setSsMode ( bool setSsMode )
{

	uint16_t currentReg = getControlRegister ();
	m_setSsMode = setSsMode;

	uint16_t updatedCtrlReg = currentReg;
	updatedCtrlReg ^= (-setSsMode ^ updatedCtrlReg) & (1 << 5);

	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_W_CTRL,
	    {0, 0, (uint8_t)(updatedCtrlReg >> 8), (uint8_t)updatedCtrlReg} );
	m_synchronousService.sendAndWaitReply( request, 1000 );

}

int Sca::Spi::getSpiMode ()
{
	uint16_t ctrlReg = getControlRegister();
	bool invClk = (ctrlReg >> 15) & 1;
	bool rxEdge = (ctrlReg >> 1) & 1;
	bool txEdge = (ctrlReg >> 2) & 1;

	int spiMode;

	if (invClk && rxEdge && txEdge)
		spiMode = 3;
	else if ( invClk && ~rxEdge && ~txEdge )
		spiMode = 2;
	else if ( ~invClk && rxEdge && txEdge )
		spiMode = 1;
	else if ( ~invClk && ~rxEdge && ~txEdge )
		spiMode = 0;

	return spiMode;
}

void Sca::Spi::setSpiMode ( uint8_t spiMode )
{

	/*	TODO Define SPI mode in a better way
	 *
	 *  uint16_t ctrlReg = getControlRegister();
	 *	bool invClk = (ctrlReg >> 15) & 1;
	 *	bool rxEdge = (ctrlReg >> 1) & 1;
	 *	bool txEdge = (ctrlReg >> 2) & 1;
	 */

}

uint16_t Sca::Spi::getControlRegister()
{
	Request request(
	    ::Sca::Constants::ChannelIds::SPI,
	    ::Sca::Constants::Commands::SPI_R_CTRL,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
	// reply 4 could be 5 and vice versa
	return (reply[6] << 8) | reply[7] ;
}

void Sca::Spi::writeSpiSlaveChunk ( std::vector<uint8_t>::iterator &dataIter )
{

    if (m_transmissionBitLength > 96)
    {
        Request request4(
        	::Sca::Constants::ChannelIds::SPI,
        	::Sca::Constants::Commands::SPI_W_MOSI3,
        	{ *++dataIter, *--dataIter, *++++++dataIter, *--dataIter } );
        m_synchronousService.sendAndWaitReply( request4, 1000 );

        *++++dataIter;

    }

    if (m_transmissionBitLength > 64)
    {
        Request request3(
        	::Sca::Constants::ChannelIds::SPI,
        	::Sca::Constants::Commands::SPI_W_MOSI2,
        	{ *++dataIter, *--dataIter, *++++++dataIter, *--dataIter } );
        m_synchronousService.sendAndWaitReply( request3, 1000 );

        *++++dataIter;

    }

    if (m_transmissionBitLength > 32)
	{
        Request request2(
        	::Sca::Constants::ChannelIds::SPI,
        	::Sca::Constants::Commands::SPI_W_MOSI1,
        	{ *++dataIter, *--dataIter, *++++++dataIter, *--dataIter } );
        m_synchronousService.sendAndWaitReply( request2, 1000 );

        *++++dataIter;

	}

    Request request1(
    	::Sca::Constants::ChannelIds::SPI,
    	::Sca::Constants::Commands::SPI_W_MOSI0,
		 { *++dataIter, *--dataIter, *++++++dataIter, *--dataIter } );
    m_synchronousService.sendAndWaitReply( request1, 1000 );


    Request requestGo(
    	::Sca::Constants::ChannelIds::SPI,
    	::Sca::Constants::Commands::SPI_GO,
    	{} );
    m_synchronousService.sendAndWaitReply( requestGo, 1000 );

}

unsigned int Sca::Spi::getSpiIterations ( std::vector<uint8_t> &payload )
{

	if ( (payload.size() * 8) % m_transmissionBitLength > 7 )
		throw std::runtime_error("Payload size is not allowed: "+lexical_cast<std::string>(payload.size())+" byte(s)" );

	return (payload.size() * 8) / m_transmissionBitLength;
}

} /* namespace Sca */

