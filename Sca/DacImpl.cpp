#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>
#include <fstream>
#include <iterator>


using boost::lexical_cast;
namespace Sca
{


/**
* DAC Commands
*/

::Sca::Constants::Commands Sca::Dac::getReadCommandFromRegister(::Sca::Constants::Dac reg)
{
	switch(reg)
	{
	case ::Sca::Constants::Dac::DAC_A: return ::Sca::Constants::Commands::DAC_R_A; break;
	case ::Sca::Constants::Dac::DAC_B: return ::Sca::Constants::Commands::DAC_R_B; break;
	case ::Sca::Constants::Dac::DAC_C: return ::Sca::Constants::Commands::DAC_R_C; break;
	case ::Sca::Constants::Dac::DAC_D: return ::Sca::Constants::Commands::DAC_R_D; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
::Sca::Constants::Commands Sca::Dac::getWriteCommandFromRegister(::Sca::Constants::Dac reg)
{
	switch(reg)
	{
	case ::Sca::Constants::Dac::DAC_A: return ::Sca::Constants::Commands::DAC_W_A; break;
	case ::Sca::Constants::Dac::DAC_B: return ::Sca::Constants::Commands::DAC_W_B; break;
	case ::Sca::Constants::Dac::DAC_C: return ::Sca::Constants::Commands::DAC_W_C; break;
	case ::Sca::Constants::Dac::DAC_D: return ::Sca::Constants::Commands::DAC_W_D; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
void Sca::Dac::writeRegister(::Sca::Constants::Dac reg, uint8_t data)
{
	
	boost::lock_guard<boost::mutex> lock( m_single_access_mutex_DAC );
	Request request(
			::Sca::Constants::ChannelIds::DAC,
			getWriteCommandFromRegister(reg),
			{0,data,0,0}
			 );
	m_synchronousService.sendAndWaitReply( request, 1000 );
}

uint8_t Sca::Dac::readRegister(::Sca::Constants::Dac reg)
{
	Request request(
			::Sca::Constants::ChannelIds::DAC,
			getReadCommandFromRegister(reg),
			{}
			 );

	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	return (uint8_t) reply[5];
}
float Sca::Dac::getVoltageEstimateAtRegister(::Sca::Constants::Dac reg)
{
	return 1.15*static_cast<int>(readRegister(reg)) / static_cast<int>(0xFF);
}
float Sca::Dac::getVoltageStepEstimate()
{
	return 1.15*static_cast<int>(0x01) / static_cast<int>(0xFF);
}
void Sca::Dac::incrementChannelByStep(::Sca::Constants::Dac reg)
{
	uint8_t val = readRegister(reg);
	if(val!=0xFF)
	{
		val++;
		writeRegister(reg,val);
	}
}
void Sca::Dac::decrementChannelByStep(::Sca::Constants::Dac reg)
{
	uint8_t val = readRegister(reg);
	if(val!=0x00)
	{
		val--;
		writeRegister(reg,val);
	}
}
void Sca::Dac::setVoltageOnChannel(::Sca::Constants::Dac reg, float voltage)
{
	if(voltage<0.0)
		voltage = 0.0;
	else if(voltage>1.15)
		voltage = 1.15;
	
	writeRegister(reg, voltage/1.15 * 255.0);
	
		
}

}
