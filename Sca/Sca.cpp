/*
 * Sca.cpp
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel, aikoulou, pmoschov
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>

using boost::lexical_cast;


namespace Sca
{

    void throwIfScaReplyError(const Reply& reply)
    {
    	if (reply.error())
	    THROW_WITH_ORIGIN(std::runtime_error,"SCA Error:"+translateError(static_cast<Constants::Errors>(reply.error())));
    }

    Sca::Sca(const std::string& address):
	m_backend( ::Hdlc::BackendFactory::getBackend( address ) ),
	m_synchronousService( m_backend ),
	m_spi( m_synchronousService ),
	m_gpio( m_synchronousService ),
	m_dac( m_synchronousService ),
	m_jtag (m_synchronousService ),
	m_i2c (m_synchronousService ),
	m_cachedScaId(0xffffffff) // this will be completed in the ctr by call to readScaId
    {
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::NODE );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::ADC );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::SPI );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::GPIO );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::DAC );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::JTAG );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C0 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C1 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C2 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C3 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C4 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C5 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C6 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C7 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C8 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2C9 );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CA );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CB );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CC );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CD );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CE );
	m_synchronousService.registerChannel( ::Sca::Constants::ChannelIds::I2CF );
	setChannelEnabled( ::Sca::Constants::ChannelIds::ADC, true ); // NEVER skip it - ADC channel provides SCA ID readout
	setChannelEnabled( ::Sca::Constants::ChannelIds::SPI, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::GPIO, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::DAC, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::JTAG, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C0, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C1, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C2, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C3, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C4, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C5, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C6, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C7, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C8, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2C9, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CA, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CB, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CC, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CD, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CE, true );
	setChannelEnabled( ::Sca::Constants::ChannelIds::I2CF, true );

	try
	{
	    m_cachedScaId = this->readChipId();
	    // successful: we have SCA V2
	    m_adc = std::unique_ptr<Adc> ( new AdcV2 ( m_synchronousService ));
	}
	catch (...)
	{
	    LOG(Log::WRN) << "Detected SCA V1. The ADC support for ADC v1 is experimental. Please contact CPPM for support";
	    m_adc = std::unique_ptr<Adc> ( new AdcV1 (m_synchronousService ));
	    m_spi.setScaV1(true);
	}
    }

    Sca::~Sca()
    {

    }

    // note: here is the mapping of channel enable to bits:
    // CRB: channels 1-7, CRC: channels 8-15, CRD: channels 16-21

    uint8_t Sca::getNodeControlCommand ( int scaChannel, bool forWrite )
    {
	if (scaChannel >= 1 && scaChannel <= 7)
	{
	    if (forWrite)
		return ::Sca::Constants::Commands::NC_W_CRB;
	    else
		return ::Sca::Constants::Commands::NC_R_CRB;
	}
	else if (scaChannel >= 8 && scaChannel <= 15)
	{
	    if (forWrite)
		return ::Sca::Constants::Commands::NC_W_CRC;
	    else
		return ::Sca::Constants::Commands::NC_R_CRC;
	}
	else if (scaChannel >= 16 && scaChannel <= 21)
	{
	    if (forWrite)
		return ::Sca::Constants::Commands::NC_W_CRD;
	    else
		return ::Sca::Constants::Commands::NC_R_CRD;
	}
	else
	    throw std::runtime_error ("Argument out of range: scaChannel");
    }

    void Sca::setChannelEnabled ( int scaChannel, bool enabled )
    {
	// get channel enable mask:
	uint8_t readCommand = getNodeControlCommand( scaChannel, /*forWrite*/ false );
	Request request(
	    ::Sca::Constants::ChannelIds::NODE,
	    readCommand,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);

	unsigned int bit = scaChannel & 0x07;
	uint8_t mask = reply[5];
	LOG(Log::TRC) << "Current mask is: " << std::hex << (unsigned int)mask;
	if (enabled)
	    mask |= 1 << bit;
	else
	    mask &= ~ ( 1 << bit );
	int writeCommand = getNodeControlCommand( scaChannel, /*forWrite*/ true );

	request = Request(
	    ::Sca::Constants::ChannelIds::NODE,
	    writeCommand,
	    {0, mask} );
	reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
    }

    bool Sca::getChannelEnabled ( int scaChannel )
    {
	// get channel enable mask:
	int readCommand = getNodeControlCommand( scaChannel, /*forWrite*/ false );
	Request request(
	    ::Sca::Constants::ChannelIds::NODE,
	    readCommand,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	unsigned int bit = scaChannel & 0x07;
	uint8_t mask = reply[4];
	return mask & (1 << bit);
    }

    uint32_t Sca::readChipId()
    {
	Request request(
	    ::Sca::Constants::ChannelIds::ADC,
	    ::Sca::Constants::Commands::CTRL_R_ID,
	    {0, 0, 1, 0} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError( reply );
	uint32_t id = (reply[4] << 16) | (reply[7] << 8) | (reply[6]);
	return id;

    }

    Hdlc::BackendStatistics Sca::getBackendStatistics () const
    {
        return m_backend->getStatistics();
    }


} /* namespace Sca */


