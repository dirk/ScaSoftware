#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>
#include <fstream>
#include <iterator>


using boost::lexical_cast;
namespace Sca
{

/**
* JTAG Commands
*/
void Sca::Jtag::writeCTRL(uint16_t data)
{

    LOG(Log::TRC) << "[jtag] writing CTRL] = "<<data;
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

 Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_W_CTRL,
    {
      0,
      0,
      (uint8_t) (data >>  0),
      (uint8_t) (data >>  8)
                }
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );
}

uint16_t Sca::Jtag::readCTRL()
{
    //LOG(Log::TRC) << "[jtag] reading CTRL 11";
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_R_CTRL,
    		{}
                );
    //LOG(Log::TRC) << "[jtag] reading CTRL 22";
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
    //LOG(Log::TRC) << "[jtag] reading CTRL 33";
    return reply[6] << 0 | reply[7] << 8 ;	
}

Request Sca::Jtag::makeTDO(int ind, uint32_t data)
{

    LOG(Log::TRC) << "[jtag] preparing TDO request";
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

    //find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO3;break;
	}
    
    //make the frame
    Request request(
	::Sca::Constants::ChannelIds::JTAG,
	temp_cmd,
    {
      (uint8_t) (data >> 16),
      (uint8_t) (data >> 24),
      (uint8_t) (data >>  0),
      (uint8_t) (data >>  8)
                }
                );

    return request;
}//makeTDO

void Sca::Jtag::writeTDO(int ind, uint32_t data)
{
    LOG(Log::TRC) << "[jtag] writing TDO";
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

	//find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TDO3;break;
	}
    
	//make the frame
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {
      (uint8_t) (data >> 16),
      (uint8_t) (data >> 24),
      (uint8_t) (data >>  0),
      (uint8_t) (data >>  8)
                }
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );
}//writeTDO

Request Sca::Jtag::makeTMS(int ind, uint32_t data)
{
    LOG(Log::TRC) << "[jtag] making TMS request";
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

	//find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS3;break;
	}
    
	//make the frame
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {
      (uint8_t) (data >> 16),
      (uint8_t) (data >> 24),
      (uint8_t) (data >>  0),
      (uint8_t) (data >>  8)
                }
                );
    return request;

}//makeTMS

Request Sca::Jtag::makeTDI(int ind)
{
        //find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
        {
                case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI0;break;
                case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI1;break;
                case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI2;break;
                case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI3;break;
        }
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {}
                );

    return request;

}


void Sca::Jtag::writeTMS(int ind, uint32_t data)
{
    LOG(Log::TRC) << "[jtag] writing TMS";
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );

	//find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_W_TMS3;break;
	}
    
	//make the frame
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {
      (uint8_t) (data >> 16),
      (uint8_t) (data >> 24),
      (uint8_t) (data >>  0),
      (uint8_t) (data >>  8)
                }
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );
}//writeTMS

uint32_t Sca::Jtag::readTDI(int ind)
{
	//find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TDI3;break;
	}
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {}
                );
    
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
    return reply[4]<<16 | reply[5]<<24 | reply[6]<<0 | reply[7]<<8;	

}

uint32_t Sca::Jtag::readTMS(int ind)
{
	//find the right command
    ::Sca::Constants::Commands temp_cmd;
    switch(ind)
	{
		case 0: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TMS0;break;
		case 1: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TMS1;break;
		case 2: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TMS2;break;
		case 3: temp_cmd = ::Sca::Constants::Commands::JTAG_R_TMS3;break;
	}
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                temp_cmd,
    {}
                );
    
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
    return reply[4]<<16 | reply[5]<<24 | reply[6]<<0 | reply[7] <<8;	
}
//configuration commands
void Sca::Jtag::setTransmissionLength_Bits(int len)
{
	if(len>128 || len<1) //this ensures that we won't have unwanted bits for the next operation
	        throw_runtime_error_with_origin("JTAG length out of range [1..128]");
	
	LOG(Log::TRC) << "[jtag] setTransmissionLength_Bitstransm length = "<<len;
//            F    F    8    0
// 0xFFC0 = 1111 1111 1000 0000
//                      reset length          
	writeCTRL( (readCTRL() & 0xFF80) |  (len==128 ? 0 : len)  );
	LOG(Log::TRC) << "[jtag] setTransmissionLength_Bitstransm length = "<<len;
}
void Sca::Jtag::setRxEdge(::Sca::Constants::Jtag val)
{
	writeCTRL((readCTRL() & ~(1<<9)  ) | (val << 9) );	
}
void Sca::Jtag::setTxEdge(::Sca::Constants::Jtag val)
{
	writeCTRL((readCTRL() & ~(1<<10)  ) | (val << 10) );	
}
void Sca::Jtag::setBitTransmissionOrder(::Sca::Constants::Jtag val)
{
	writeCTRL((readCTRL() & ~(1<<11)  ) | (val << 11) );	
}
void Sca::Jtag::setTckIdleLevel(::Sca::Constants::Jtag val)
{
	writeCTRL((readCTRL() & ~(1<<14)  ) | (val << 14) );	
}
void Sca::Jtag::setFrequencyDivider(uint16_t val)
{
    //LOG(Log::INF) << "[debug] setting FREQ [div] = "<<val;
    boost::lock_guard<boost::mutex> lock( m_single_access_mutex_JTAG );
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_W_FREQ,
    { 0,
      0,
      (uint8_t) (val >>  0),
      (uint8_t) (val >>  8)
                }
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );	
}
void Sca::Jtag::setFrequency_MHz(float val)
{
	if(val>20.0) //this ensures that we won't have unwanted bits for the next operation
	        throw_runtime_error_with_origin("JTAG frequency out of range (0.0..20.0)");
	if(val<0.0) //this ensures that we won't have unwanted bits for the next operation
	        throw_runtime_error_with_origin("JTAG frequency out of range (0.0..20.0)");
	if((val<20.0 && val>10.0) || (val<10.0 && val>5.0)) //remind the user the limitations of freq divider...
		LOG(Log::INF) << "[JTAG] value not allowed. Allowed f = 20/q, where q=integer";

	setFrequencyDivider(  (uint16_t)(20.0/val-1)  );
}

int Sca::Jtag::getTransmissionLength_Bits()
{
	uint tmp = readCTRL() & 127;
	return (tmp==0? 128 : (int)tmp);
}

::Sca::Constants::Jtag Sca::Jtag::getRxEdge()
{
	uint16_t tmp = readCTRL() & (1<<9);
	return ( tmp==0 ?  ::Sca::Constants::Jtag::RxEdgeRising : ::Sca::Constants::Jtag::RxEdgeFalling);	
}

::Sca::Constants::Jtag Sca::Jtag::getTxEdge()
{
	uint16_t tmp = readCTRL() & (1<<10);
	return ( tmp==0 ?  ::Sca::Constants::Jtag::TxEdgeRising : ::Sca::Constants::Jtag::TxEdgeFalling);	
}

::Sca::Constants::Jtag Sca::Jtag::getBitTransmissionOrder()
{
	uint16_t tmp = readCTRL() & (1<<11);
	return ( tmp==0 ?  ::Sca::Constants::Jtag::MSB_LSB : ::Sca::Constants::Jtag::LSB_MSB)	;
}

::Sca::Constants::Jtag Sca::Jtag::getTckIdleLevel()
{
	uint16_t tmp = readCTRL() & (1<<14);
	return ( tmp==0 ?  ::Sca::Constants::Jtag::TCK_Idle_High : ::Sca::Constants::Jtag::TCK_Idle_Low);	
}

uint16_t Sca::Jtag::getFrequencyDivider()
{
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_R_FREQ,
    {}
                );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
    
    return reply[6]<<0 | reply[7]<<8;	
}

float Sca::Jtag::getFrequency_MHz()
{
	return 2.0*10.0/(getFrequencyDivider()+1.0);
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


//transmission commands

void Sca::Jtag::sendByteVector(std::vector<uint8_t> data)
{

writeTMS(0,0x0);
writeTMS(1,0x0);
writeTMS(2,0x0);
writeTMS(3,0x0);

	int transmissionLength = getTransmissionLength_Bits();
		LOG(Log::INF) << "transm length = "<<transmissionLength;
	if(transmissionLength %8 != 0) //make sure transmission length is multiple of 8
	{
		
	        throw_runtime_error_with_origin("transm length not a multiple of 8bits");
	}

	int transmissionLengthBytes = transmissionLength/8;// min=1, max=16
	int noOfBytesFilled = 0;
	uint32_t tdo0Buffer(0),tdo1Buffer(0),tdo2Buffer(0),tdo3Buffer(0);
	std::vector<uint8_t>::iterator it = data.begin();
	std::vector<uint8_t>::iterator itEnd = data.end();

	LOG(Log::INF) << "vector size = "<<data.size();

	int myindex=0;
	while(it != itEnd)
	{
		if(myindex % 5000 == 0)
		LOG(Log::INF) << "[jtag]" << myindex<<" / "<<data.size()<<" # "<<100.0*myindex/data.size()<<"% :: data from .bit file: "<<*it;
		
		//if no bytes filled (first iteration)
		
		if(noOfBytesFilled<4)
{

//if we are in the last byte, we must make the last TMS a 1
if(it+1==itEnd)
writeTMS(3,0x80); // 0x80 = 1000 0000

			tdo3Buffer = tdo3Buffer<<8 | *it;
}
		else if(noOfBytesFilled<8){

//if we are in the last byte, we must make the last TMS a 1
if(it+1==itEnd)
writeTMS(2,0x80); // 0x80 = 1000 0000


			tdo2Buffer = tdo2Buffer<<8 | *it;

}
		else if(noOfBytesFilled<12){

//if we are in the last byte, we must make the last TMS a 1
if(it+1==itEnd)
writeTMS(1,0x80); // 0x80 = 1000 0000

			tdo1Buffer = tdo1Buffer<<8 | *it;
}
		else if(noOfBytesFilled<16){

//if we are in the last byte, we must make the last TMS a 1
if(it+1==itEnd)
writeTMS(0,0x80); // 0x80 = 1000 0000

			tdo0Buffer = tdo0Buffer<<8 | *it;
}

		noOfBytesFilled++;
		if(noOfBytesFilled > 16) //should never happen
	        	throw_runtime_error_with_origin("noOfBytesFilled has unexpected value!!!");
		
		if( noOfBytesFilled ==  transmissionLengthBytes)
		{
			//here we have filled the TDO with the right order
			//and with the exact amount of bytes as the transmission length
			//so we write the TDO
			writeTDO(0,tdo0Buffer);
			writeTDO(1,tdo1Buffer);
			writeTDO(2,tdo2Buffer);
			writeTDO(3,tdo3Buffer);
			//and just send the payload
			JTAG_GO();
LOG(Log::INF) << "[jtag] sending data: " << tdo0Buffer << " " << tdo1Buffer << " " << tdo2Buffer  << " " << tdo3Buffer;
			//probably no reason to do JTAG_RESET
				
			noOfBytesFilled=0;
			tdo3Buffer=0;
			tdo2Buffer=0;
			tdo1Buffer=0;
			tdo0Buffer=0;
		}
		myindex++;

	
		++it;
	}
}//sendByteVector
void Sca::Jtag::sendFile(std::string filename)
{    
/*
	std::ifstream input( filename, std::ios::binary );
	// copies all data into buffer
	std::vector<char> buffer(  (
	std::istreambuf_iterator<char>(input)), 
	(std::istreambuf_iterator<char>())  );
*/

    // open the file:
    std::ifstream file(filename, std::ios::binary);
    if(!file)
        throw_runtime_error_with_origin("file doesn't exist");
    // Stop eating new lines in binary mode!!!
    file.unsetf(std::ios::skipws);
    // get its size:
    std::streampos fileSize;
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);
    // reserve capacity
    std::vector<uint8_t> vec;
    vec.reserve(fileSize);
    // read the data:
    vec.insert(vec.begin(),
               std::istream_iterator<uint8_t>(file),
               std::istream_iterator<uint8_t>());


//TODO make this dynamic (...)
	setTransmissionLength_Bits(128); 

	sendByteVector(vec);
}

void Sca::Jtag::programFPGAwithFile(std::string fileName)
{

// try to replicate the sequence as defined in the Kintex7 giude
// https://www.xilinx.com/support/documentation/user_guides/ug470_7Series_Config.pdf
// page 174

//step by step we add the required bits, and then we clock them out all together.
//if we don't care about TDO data, then we just clock out TMS
//if we also send TDO data, then we set TMS+TDO at the same time 

int bitsToSend=0;

LOG(Log::INF) << "Starting FPGA programming";
LOG(Log::TRC) << "step1";

//1 power up. Reset jtag  FSM (x5 TMS=1)
for(int i=0;i<5;i++) clockOutSingleTMS(true);
LOG(Log::TRC) << "step2";
//2 goto RTI state
clockOutSingleTMS(false);
LOG(Log::TRC) << "step3";
//3 goto SELECT-IR
clockOutSingleTMS(true);
clockOutSingleTMS(true);
//4 goto SHIFT-IR
clockOutSingleTMS(false);
clockOutSingleTMS(false);
//5 start JPROGRAM instruction
LOG(Log::INF) << "Sending JPROGRAM";
sendUpto8Bits_TMS_Length(0xB,false,5);
//6 load MSB of JPROGRAM instr when exiting SHIFT-IR
sendUpto8Bits_TMS_Length(0x0,true,1);
//7 goto TLR state
for(int i=0;i<5;i++) clockOutSingleTMS(true);
//8 goto RTI state
//need to send TMS=0 for 10ms (see footnote in link above)
clockOutTMSForTime_ms(false,10);
//9 start CFG_IN instruction
sendUpto8Bits_TMS_Length(0x5,false,5);
//10 load MSB of CFG_IN instr when exiting SHIFT-IR
sendUpto8Bits_TMS_Length(0x0,true,1);
//11 goto SELECT-DR
clockOutSingleTMS(true);
clockOutSingleTMS(true);
//12 goto SHIFT-DR
clockOutSingleTMS(false);
clockOutSingleTMS(false);
//13 shift in the bitstream
LOG(Log::INF) << "Shifting in the bitstream";
sendFile(fileName);
//14 last bit of the bitstream here!!!
//...
LOG(Log::INF) << "Shifting in the bitstream...done!";
//15 goto UPDATE-DR
clockOutSingleTMS(true);
//16 goto RTI 
clockOutSingleTMS(false);
//17 goto SELECT-IR
clockOutSingleTMS(true);
clockOutSingleTMS(true);
//18 goto SHIFT-IR
clockOutSingleTMS(false);
clockOutSingleTMS(false);
//19 start JSTART instruction (OPTIONAL???)
LOG(Log::INF) << "Sending JSTART";
sendUpto8Bits_TMS_Length(0xC,false,5);
//20 load MSB of JSTART instr
sendUpto8Bits_TMS_Length(0x0,true,1);
//21 goto UPDATE-IR
clockOutSingleTMS(true);
//22 goto RTI +clock the startup sequence by applying min 2000 tck
LOG(Log::INF) << "Going to RTI";
writeTMS(0,0x0);
writeTMS(1,0x0);
writeTMS(2,0x0);
writeTMS(3,0x0);
setTransmissionLength_Bits(100); 
for(int i=0;i<21;i++) JTAG_GO();//so we send 21x100=2100 times a TCK
//23 goto TLR
clockOutSingleTMS(true);
clockOutSingleTMS(true);
clockOutSingleTMS(true);
//device should be functional now...
LOG(Log::INF) << "Fpga should be programmed now";


}//programFPGAwithFile

void Sca::Jtag::clockOutTMSForTime_ms(bool tmsVal, uint t_ms)
{
//hardcode transmission frequency for this function?
	setFrequency_MHz(1.0);

//writeTMS to tmsVal
	writeTMS(0, tmsVal ? 0xFFFFFFFF : 0x00000000);
	writeTMS(1, tmsVal ? 0xFFFFFFFF : 0x00000000);
	writeTMS(2, tmsVal ? 0xFFFFFFFF : 0x00000000);
	writeTMS(3, tmsVal ? 0xFFFFFFFF : 0x00000000);


//settransmissionlength
	setTransmissionLength_Bits(100);  

//set timer
	//auto duration = now.time_since_epoch();
	//auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
	
	//while(millis < t_ms)
	{
		JTAG_GO();
		//millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
	}

//now we have sent the requested TMS val for the requested t in ms


}//clockOutTMSForTime_ms

void Sca::Jtag::sendUpto8Bits_TMS_Length(uint8_t word,bool tmsVal, uint length)
{
//this function is useful only for FPGA programming, where usually one needs to send 5bit words
//TODO optimize,compactize with other functions

//check that we are at 8bits or 
	if(length>8) //make sure transmission length is <=8
	        throw_runtime_error_with_origin("max length = 8bits for this function!");

//reset the 1,2,3 tdo and TMS
	writeTDO(1,0x0);
	writeTDO(2,0x0);
	writeTDO(3,0x0);
	writeTMS(1,0x0);
	writeTMS(2,0x0);
	writeTMS(3,0x0);

//set the TMS to value tmsVal
	writeTMS(0, (tmsVal ? 0xFF : 0x00) );

//set the TDO to word
	writeTDO(0, word);

//settransmissionlength
	setTransmissionLength_Bits(length);  

//send it!
	JTAG_GO();

}//sendUpto8Bits_TMS_Length
void Sca::Jtag::clockOutSingleTMS(bool val)
{
//to send a single bit with JTAG
// 1) set transmission length to 1
// 2) reset TMSbuffers. Set the LSB of TMS0
// 3) send it!

LOG(Log::TRC) << "setting trans length to 1";
	setTransmissionLength_Bits(1);  
LOG(Log::TRC) << "done";

	writeTMS(0, (val ? 0x01 : 0x00 )  );
	writeTMS(1,0x0);
	writeTMS(2,0x0);
	writeTMS(3,0x0);  

	JTAG_GO();

}//clockOutSingleTMS

void Sca::Jtag::clockOutTMS_times(bool val, int howmany)
{

if(howmany<1)
	throw_runtime_error_with_origin("JTAG length out of range [1..128]");

while(howmany>127)
{
	LOG(Log::TRC) << "setting trans length to 128";
	setTransmissionLength_Bits(128);  

	writeTMS(0, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(1, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(2, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(3, (val ? 0xFFFFFFFF : 0x0 )  );

	JTAG_GO();

	howmany-=128;
}

if(howmany>0)
{
	setTransmissionLength_Bits(howmany);  

	writeTMS(0, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(1, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(2, (val ? 0xFFFFFFFF : 0x0 )  );
	writeTMS(3, (val ? 0xFFFFFFFF : 0x0 )  );

	JTAG_GO();

}
LOG(Log::TRC) << "done";

}//clockOutSingleTMS

void Sca::Jtag::scanChain()
{

LOG(Log::TRC) << "[JTAG] Reading TDI: " << readTDI(0)<< readTDI(1) << readTDI(2)<<readTDI(3);

LOG(Log::TRC) << "[JTAG] TLR";
//goto reset state
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);
LOG(Log::TRC) << "[JTAG] ShiftIR";
//goto Shift-IR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(true);//Sel IR
clockOutSingleTMS(false);//capture ID
clockOutSingleTMS(false);//shift IR
LOG(Log::TRC) << "[JTAG] SendMany";
//send many ones
for(int i=0;i<999;i++)
	sendUpto8Bits_TMS_Length(0x1,false,1);//1
//last one is with TMS active
sendUpto8Bits_TMS_Length(0x1,true,1);
LOG(Log::TRC) << "[JTAG] ShiftDR";
//goto Shift-DR
clockOutSingleTMS(true);
clockOutSingleTMS(true);
clockOutSingleTMS(false);
clockOutSingleTMS(false);
LOG(Log::TRC) << "[JTAG] 0s";
//send 0s to flush
for(int i=0;i<999;i++)
	sendUpto8Bits_TMS_Length(0x1,false,1);

LOG(Log::TRC) << "[JTAG] Reading TDI0: " << readTDI(0);
LOG(Log::TRC) << "[JTAG] Reading TDI1: " << readTDI(1);
LOG(Log::TRC) << "[JTAG] Reading TDI2: " << readTDI(2);
LOG(Log::TRC) << "[JTAG] Reading TDI3: " << readTDI(3);

//send ones until we receive one back
LOG(Log::TRC) << "[JTAG] 1s";
for(int i=0;i<999;i++)
 {
LOG(Log::TRC) << "[JTAG] Reading TDI0: " << readTDI(0);
LOG(Log::TRC) << "[JTAG] Reading TDI1: " << readTDI(1);
LOG(Log::TRC) << "[JTAG] Reading TDI2: " << readTDI(2);
LOG(Log::TRC) << "[JTAG] Reading TDI3: " << readTDI(3);
sendUpto8Bits_TMS_Length(0x1,false,1);
 }

}


void Sca::Jtag::getFpgaId()
{
//we will try to get the FPGA in the state to shift out its ID
//IDCODE Read/Write address:01100 Device ID Register 

LOG(Log::TRC) << "[JTAG] PreReading TDI: " <<std::hex << readTDI(3)<< readTDI(2) << readTDI(1)<< readTDI(0);

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);
//goto Shift-IR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(true);//Sel IR
clockOutSingleTMS(false);//capture ID
clockOutSingleTMS(false);//shift IR

LOG(Log::TRC) << "[JTAG] Send IDCOD instr";



//send IDCODES instruction (LSB first)
sendUpto8Bits_TMS_Length(0x0,false,1);//0
sendUpto8Bits_TMS_Length(0x0,false,1);//0
sendUpto8Bits_TMS_Length(0x1,false,1);//1
sendUpto8Bits_TMS_Length(0x1,false,1);//1
sendUpto8Bits_TMS_Length(0x0,true,1);//0 (TMS=1 for MSB of instruction)



LOG(Log::TRC) << "[JTAG] TLR";

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);



LOG(Log::TRC) << "[JTAG] TLR";
//go to TLR (test logic reset)cha
for(int i=0;i<5;i++)
{
	clockOutSingleTMS(true);

}

LOG(Log::TRC) << "[JTAG] Shift-DR";
//goto shift-DR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(false);//capture DR
//for(int i=0;i<200;i++)
//{
//clockOutSingleTMS(false);//shift DR
clockOutTMS_times(false,32);//shift DR
LOG(Log::TRC) << "[JTAG] Reading TDI: " <<std::hex << readTDI(3)<< readTDI(2) << readTDI(1)<< readTDI(0);


//}


}

void Sca::Jtag::getFpgaFuseDna()
{
//we will try to get the FPGA in the state to shift out its ID
//IDCODE Read/Write address:01100 Device ID Register 

LOG(Log::TRC) << "PreReading TDI: " << readTDI(0);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(1);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(2);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(3);

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);
//goto Shift-IR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(true);//Sel IR
clockOutSingleTMS(false);//capture ID
clockOutSingleTMS(false);//shift IR


//send FUSE_DNA 110010 instruction (LSB first)
sendUpto8Bits_TMS_Length(0x0,false,1);
sendUpto8Bits_TMS_Length(0x1,false,1);
sendUpto8Bits_TMS_Length(0x0,false,1);
sendUpto8Bits_TMS_Length(0x0,false,1);
sendUpto8Bits_TMS_Length(0x1,true,1);
sendUpto8Bits_TMS_Length(0x1,true,1);//(TMS=1 for MSB of instruction)



//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);


LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(0);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(1);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(2);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(3);

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);

//goto shift-DR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(false);//capture DR
clockOutSingleTMS(false);//shift DR


LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(0);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(1);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(2);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(3);

}

void Sca::Jtag::getFpgaXscDna()
{
//we will try to get the FPGA in the state to shift out its ID
//IDCODE Read/Write address:01100 Device ID Register 

LOG(Log::TRC) << "PreReading TDI: " << readTDI(0);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(1);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(2);
LOG(Log::TRC) << "PreReading TDI: " << readTDI(3);

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);
//goto Shift-IR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(true);//Sel IR
clockOutSingleTMS(false);//capture ID
clockOutSingleTMS(false);//shift IR

//send XSC_DNA 010111 instruction (LSB first)
sendUpto8Bits_TMS_Length(0x1,false,1);
sendUpto8Bits_TMS_Length(0x1,false,1);
sendUpto8Bits_TMS_Length(0x1,false,1);
sendUpto8Bits_TMS_Length(0x0,false,1);
sendUpto8Bits_TMS_Length(0x1,true,1);
sendUpto8Bits_TMS_Length(0x0,true,1);//(TMS=1 for MSB of instruction)




//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);


LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(0);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(1);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(2);
LOG(Log::TRC) << "MidReading TDI: " << std::hex <<readTDI(3);

//go to TLR (test logic reset)
for(int i=0;i<5;i++)
	clockOutSingleTMS(true);

//goto shift-DR
clockOutSingleTMS(false);//RTI
clockOutSingleTMS(true);//Sel DR
clockOutSingleTMS(false);//capture DR
clockOutSingleTMS(false);//shift DR


LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(0);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(1);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(2);
LOG(Log::TRC) << "EndReading TDI: " << std::hex <<readTDI(3);

}


void Sca::Jtag::reset()
{
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_ARESET,
    {}
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );
    
}

Request Sca::Jtag::makeGO()
{
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_GO,
    {}
                );
    return request;
    
}
void Sca::Jtag::JTAG_GO()
{
    Request request(
                ::Sca::Constants::ChannelIds::JTAG,
                ::Sca::Constants::Commands::JTAG_GO,
    {}
                );
    m_synchronousService.sendAndWaitReply( request, 1000 );
    
}

}//namespace Sca
