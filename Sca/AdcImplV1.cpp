/*
 * AdcImplV1.cpp, spawned from ADC implementation for SCA by pnikiel
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>

using boost::lexical_cast;


uint16_t Sca::AdcV1::convertChannel(uint8_t channelId)
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

uint16_t Sca::AdcV1::go ()
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

void Sca::AdcV1::setInputLine ( unsigned int inputLine )
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

unsigned int Sca::AdcV1::getInputLine ()
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

// @param line -1 to disable at all, 0-31 for a chosen inputLine
void Sca::AdcV1::setCurrentSourceLine ( int inputLine )
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

uint32_t Sca::AdcV1::getGainCalibration()
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

// NOTE: it's not understood yet what should be the format of the calibration data. Therefore
// the signature of this function is likely to change soon
void Sca::AdcV1::setGainCalibration(uint32_t x)
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

// @return -1 if no current source enabled or 0-31 for given current source enabled
int Sca::AdcV1::getCurrentSourceLine ()
{
    throw_runtime_error_with_origin("not-implemented-for-SCA-v1: ask CPPM");
}

