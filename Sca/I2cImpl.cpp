/*
 * I2cImpl.cpp
 *
 *  Created on: Mar 23, 2017
 *      Author: Paris Moschovakos
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>
#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>
#include <LogIt.h>

using boost::lexical_cast;

namespace Sca
{

uint8_t Sca::I2c::getControlRegister( uint8_t i2cMaster )
{

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_R_CTRL,
	    {} );

	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);

	return reply[5];
}

void Sca::I2c::setControlRegister ( uint8_t i2cMaster, uint8_t ctrlReg )
{

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
	    { 0, 0, ctrlReg } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request, 1000 ));

}

void Sca::I2c::setFrequency ( uint8_t i2cMaster, uint16_t frequency )
{

	// Allowed values are 100, 200, 400 and 1000 in KHz

	LOG(Log::TRC) << "Setting frequency to " << (int)(frequency);
	uint8_t freqCtrlMask = 0b00;

	switch (frequency) {
		case 100:
			freqCtrlMask = 0b00;
			break;
		case 200:
			freqCtrlMask = 0b01;
			break;
		case 400:
			freqCtrlMask = 0b10;
			break;
		case 1000:
			freqCtrlMask = 0b11;
			break;
		default:
			throw std::invalid_argument( "Frequency is not valid" );
			break;
	}

	m_frequency = freqCtrlMask;

	uint8_t currentReg = getControlRegister( i2cMaster );
	uint8_t updatedCtrlReg = (freqCtrlMask << 0) | ( currentReg & 0xFC );

	uint8_t data[2] = {0, 0};
	data[1] = updatedCtrlReg;

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
		 data, sizeof(data) );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request, 1000 ));

}

void Sca::I2c::setTransmissionByteLength ( uint8_t i2cMaster, uint8_t transmissionByteLength )
{

	LOG(Log::TRC) << "Setting byte length to " << std::hex << (int)transmissionByteLength;

	if ( transmissionByteLength > 16 || transmissionByteLength <= 0 )
	    throw std::out_of_range( "setTransmissionByteLength: argument out of range: " + lexical_cast<std::string>( (int)transmissionByteLength ) );

	m_nbyte = transmissionByteLength;

	uint8_t currentReg = getControlRegister( i2cMaster );
	uint8_t updatedCtrlReg = (transmissionByteLength << 2) | ( currentReg & 0x83 );

	Request request(
			i2cMaster,
			::Sca::Constants::Commands::I2C_W_CTRL,
			 { 0, updatedCtrlReg, 0, 0 } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request, 1000 ));

}

void Sca::I2c::setSclMode ( uint8_t i2cMaster, bool sclMode )
{

	LOG(Log::TRC) << "Setting SCL mode to " << std::hex << sclMode;

	m_sclmode = sclMode;

	uint8_t currentReg = getControlRegister( i2cMaster );

	uint8_t updatedCtrlReg = (sclMode << 7) | ( currentReg & 0x7f );

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_W_CTRL,
	    { 0, updatedCtrlReg, 0, 0 } );
	throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request, 1000 ));

}

uint8_t Sca::I2c::getStatusRegister( uint8_t i2cMaster )
{
	m_lasti2cMaster = i2cMaster;

	Request request(
			i2cMaster,
	    ::Sca::Constants::Commands::I2C_R_STR,
	    {} );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);

	return reply[5] ;
}

Sca::I2c::statusRegister Sca::I2c::getStatus( uint8_t i2cMaster, uint8_t statusReg )
{

	if ( statusReg == 255 )
		statusReg = getStatusRegister( i2cMaster );

	statusRegister statusRegisterContents;

	statusRegisterContents.successfulTransaction = ( statusReg >> 2 ) & 1;
	statusRegisterContents.sdaLineLevelError = ( statusReg >> 3 ) & 1;
	statusRegisterContents.invalidCommandSent = ( statusReg >> 5 ) & 1;
	statusRegisterContents.lastOperationNotAcknowledged = ( statusReg >> 6 ) & 1;

	return statusRegisterContents;
}

void Sca::I2c::setDataRegister ( uint8_t i2cMaster, std::vector<uint8_t> &data )
{

	if ( data.size() > 16 || data.size() < 1 )
		throw std::runtime_error("Data size is not allowed: " + lexical_cast<std::string>( data.size() ) + " byte(s)" );

    if ( data.size() > 12 )
    {

        Request request4(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA3,
			 { data[13], data[12], data[15], data[14] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request4, 1000 ));
    }

    if ( data.size() > 8 )
    {
        Request request3(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA2,
        	{ data[9], data[8], data[11], data[10] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request3, 1000 ));

    }

    if ( data.size() > 4 )
	{
        Request request2(
        		i2cMaster,
        	::Sca::Constants::Commands::I2C_W_DATA1,
        	{ data[5], data[4], data[7], data[6] } );
        throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request2, 1000 ));

	}

    Request request1(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_W_DATA0,
		 { data[1], data[0], data[3], data[2] } );
    throwIfScaReplyError(m_synchronousService.sendAndWaitReply( request1, 1000 ));

}

std::vector<uint8_t> Sca::I2c::getDataRegister ( uint8_t i2cMaster )
{

    Request request1(
    		i2cMaster,
			::Sca::Constants::Commands::I2C_R_DATA3,
			 {} );
    Reply reply1 = m_synchronousService.sendAndWaitReply( request1, 1000 );
    throwIfScaReplyError(reply1);

    std::vector<uint8_t> reply{reply1[5], reply1[4], reply1[7], reply1[6]};

    if ( m_nbyte > 4 )
	{
        Request request2(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA2,
				 {} );
        Reply reply2 = m_synchronousService.sendAndWaitReply( request2, 1000 );
        throwIfScaReplyError(reply2);

        reply.push_back(reply2[5]);
        reply.push_back(reply2[4]);
        reply.push_back(reply2[7]);
        reply.push_back(reply2[6]);

	}

    if ( m_nbyte > 8 )
    {
        Request request3(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA1,
				 {} );
        Reply reply3 = m_synchronousService.sendAndWaitReply( request3, 1000 );
        throwIfScaReplyError(reply3);

        reply.push_back(reply3[5]);
        reply.push_back(reply3[4]);
        reply.push_back(reply3[7]);
        reply.push_back(reply3[6]);
    }

    if ( m_nbyte > 12 )
    {

        Request request4(
        		i2cMaster,
				::Sca::Constants::Commands::I2C_R_DATA0,
				 {} );
        Reply reply4 = m_synchronousService.sendAndWaitReply( request4, 1000 );
        throwIfScaReplyError(reply4);

        reply.push_back(reply4[5]);
        reply.push_back(reply4[4]);
        reply.push_back(reply4[7]);
        reply.push_back(reply4[6]);
    }

    return reply;
}

std::vector<uint8_t> Sca::I2c::readSingleByte7bit ( uint8_t i2cMaster, uint8_t address )
{

	if ( address > 127 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	setTransmissionByteLength( i2cMaster, ::Sca::Constants::I2c::I2C_NBYTE_1 );

	uint8_t formattedAddressD0 = address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_S_7B_R,
			 { 0, formattedAddressD0, 0, 0 } );

	Reply reply1 = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5], reply1[4]};

	return reply;
}

uint8_t Sca::I2c::writeSingleByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t data )
{

	uint8_t formattedaddressD0 = address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_S_7B_W,
		 { data, formattedaddressD0, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> Sca::I2c::readMultiByte7bit ( uint8_t i2cMaster, uint8_t address, uint8_t nbytes )
{

	if ( address > 127 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	uint8_t formattedAddressD0 = address; // We dont have control over the R/W bit

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_M_7B_R,
			 { 0, formattedAddressD0, 0, 0 } );
	Reply reply1 = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5]};
	std::vector<uint8_t> data = getDataRegister( i2cMaster );

	reply.insert(std::end(reply), std::begin(data), std::end(data));

	return reply;
}

uint8_t Sca::I2c::writeMultiByte7bit ( uint8_t i2cMaster, uint8_t address, std::vector<uint8_t> &data )
{

	setDataRegister( i2cMaster, data );

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_M_7B_W,
		 { 0, address, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> Sca::I2c::readSingleByte10bit ( uint8_t i2cMaster, uint16_t address )
{

	if ( address > 1023 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	setTransmissionByteLength( i2cMaster, ::Sca::Constants::I2c::I2C_NBYTE_1 );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_S_10B_R,
			 { formattedaddressD1, formattedAddressD0, 0, 0 } );

	Reply reply1 = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5], reply1[4]};

	return reply;
}

uint8_t Sca::I2c::writeSingleByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t data )
{

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_S_10B_W,
		 { formattedaddressD1, formattedAddressD0, 0, data } );

    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);

    return reply[5];

}

std::vector<uint8_t> Sca::I2c::readMultiByte10bit ( uint8_t i2cMaster, uint16_t address, uint8_t nbytes )
{

	if ( address > 1023 || address < 0 )
	    throw std::out_of_range( "I2C register address: argument out of range: " + address );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

	Request request(
			i2cMaster,
	    	::Sca::Constants::Commands::I2C_M_10B_R,
			 { formattedaddressD1, formattedAddressD0, 0, 0 } );
	Reply reply1 = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply1);

	std::vector<uint8_t> reply{reply1[5]};
	std::vector<uint8_t> data = getDataRegister( i2cMaster );

	reply.insert(std::end(reply), std::begin(data), std::end(data));

	return reply;
}

uint8_t Sca::I2c::writeMultiByte10bit ( uint8_t i2cMaster, uint16_t address, std::vector<uint8_t> &data )
{

	setDataRegister( i2cMaster, data );

	uint8_t formattedAddressD0 = ::Sca::Constants::I2c::I2C_10B_ADDRESS_OFFSET + ( address >> 8 );
	uint8_t formattedaddressD1 = (uint8_t)address;

    Request request(
    		i2cMaster,
    	::Sca::Constants::Commands::I2C_M_10B_W,
		 { formattedaddressD1, formattedAddressD0, 0, 0 } );

    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);

    return reply[5];

}

uint8_t Sca::I2c::write ( uint8_t i2cMaster, bool addressingMode, uint16_t address, std::vector<uint8_t> &data )
{

	if ( data.size() > 16 || data.size() <= 0 )
	    throw std::out_of_range( "I2C write: amount of data out of range: " );

	setTransmissionByteLength( i2cMaster, (uint8_t)(data.size()) );

	uint8_t status;

	if (addressingMode)
	{
		status = data.size() > 1 ? writeMultiByte10bit( i2cMaster, address, data ) : writeSingleByte10bit( i2cMaster, address, data[0] );
	}
	else
	{
		uint8_t address8 = (uint8_t)address;
		status = data.size() > 1 ? writeMultiByte7bit( i2cMaster, address8, data ) : writeSingleByte7bit( i2cMaster, address8, data[0] );
	}

    return status;

}

std::vector<uint8_t> Sca::I2c::read ( uint8_t i2cMaster, bool addressingMode, uint16_t address, uint8_t nbytes )
{

	setTransmissionByteLength( i2cMaster, nbytes );
	std::vector<uint8_t> statusData;

	if (addressingMode)
	{
		statusData = nbytes > 1 ? readMultiByte10bit( i2cMaster, address, nbytes ) : readSingleByte10bit( i2cMaster, address );
	}
	else
	{
		uint8_t address8 = (uint8_t)address;
		statusData = nbytes > 1 ? readMultiByte7bit( i2cMaster, address8, nbytes ) : readSingleByte7bit( i2cMaster, address8 );
	}

    return statusData;

}

} /* namespace Sca */

