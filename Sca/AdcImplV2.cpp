/*
 * AdcImplV2.cpp
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel
 */

#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>
#include <Sca/ErrorTranslator.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>

using boost::lexical_cast;





uint16_t Sca::AdcV2::convertChannel(uint8_t channelId)
{
    // TODO: add caching (don't send setInputLine if that input line is already chosen)
    setInputLine (channelId);
    return go();
}

uint16_t Sca::AdcV2::go ()
{
    LOG(Log::TRC) << "adcGo";
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_GO,
              {0, 0, 1, 0} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError( reply );
    return reply[7]<<8 | reply[6];
}

void Sca::AdcV2::setInputLine ( unsigned int inputLine )
{
    LOG(Log::TRC) << "setInputLine(line=" << inputLine << ")";
    if (inputLine >= ::Sca::Constants::Specs::ADC_CHANNELS_NUM)
        throw std::runtime_error("setInputLine: argument out of range: "+lexical_cast<std::string>(inputLine) );
    // TODO: not sure about bytes mapping: need to be checked
    // TODO: this conforms rather to the old ADC
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_W_INSEL,
              {0, 0, (uint8_t)inputLine, (uint8_t)inputLine} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError( reply );
    LOG(Log::INF) << "got reply: " << reply.toString();

}

unsigned int Sca::AdcV2::getInputLine ()
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_R_INSEL,
              {} );
    // TODO: not sure about bytes mapping: need to be checked
    // TODO: this conforms rather to the old ADC
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    return reply[6];
}

// @param line -1 to disable at all, 0-31 for a chosen inputLine
void Sca::AdcV2::setCurrentSourceLine ( int inputLine )
{
    uint32_t lineBitmap = 0;
    if (inputLine >= ::Sca::Constants::Specs::ADC_CHANNELS_NUM )
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__)+": argument out of range: "+lexical_cast<std::string>(inputLine) );
    else if (inputLine >= 0)
        lineBitmap = 1 << inputLine;
    else if (inputLine >= -1)
        lineBitmap = 0;
    else
        throw std::runtime_error("setCurrentSourceLine: argument invalid: "+ lexical_cast<std::string>(inputLine) );
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_W_CUREN,
              {uint8_t(lineBitmap>>16), uint8_t(lineBitmap>>24), uint8_t(lineBitmap), uint8_t(lineBitmap>>8)} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
}

uint32_t Sca::AdcV2::getGainCalibration()
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_R_GAIN,
              {} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);
    return reply[4] << 16 | reply[5] << 24 | reply[6] | reply[7] << 8;
}

// NOTE: it's not understood yet what should be the format of the calibration data. Therefore
// the signature of this function is likely to change soon
void Sca::AdcV2::setGainCalibration(uint32_t x)
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_W_GAIN,
              {(uint8_t)(x>>24), (uint8_t)(x>>16), (uint8_t)(x>>8), (uint8_t)x} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
    throwIfScaReplyError(reply);

}

// @return -1 if no current source enabled or 0-31 for given current source enabled
int Sca::AdcV2::getCurrentSourceLine ()
{
    Request request(
            ::Sca::Constants::ChannelIds::ADC,
             ::Sca::Constants::Commands::ADC_R_CUREN,
              {} );
    Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );

    uint32_t lineBitmap = reply[4] << 16 | reply[5] << 24 | reply[6] | reply[7] << 8;
    for (int bitNo=0; bitNo < ::Sca::Constants::Specs::ADC_CHANNELS_NUM ; bitNo++)
    {
        if (lineBitmap & (1 << bitNo))
        {
            // TODO: hex would be better here
            if ( (lineBitmap & ~( 1 << bitNo )) != 0 )
                throw std::runtime_error("Can't parse the current line bitmap: more than 1 line enabled! (bitmap="+lexical_cast<std::string>(lineBitmap)+")");
            return bitNo;
        }
    }
    return -1;
}



