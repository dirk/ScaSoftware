/*
 * ErrorTranslator.cpp
 *
 *  Created on: Mar 22, 2017
 *      Author: pnikiel
 */

#include <boost/lexical_cast.hpp>

#include <Sca/ErrorTranslator.h>

namespace Sca
{

std::string translateError( Constants::Errors e )
{
	using Constants::Errors;
	switch (e)
	{
        case Errors::INVALID_COMMAND_REQUEST:
	  return "INVALID_COMMAND_REQUEST";
	case Errors::INVALID_REQUEST_LENGTH:
		return "INVALID_REQUEST_LENGTH";
	case Errors::CHANNEL_DISABLED:
		return "CHANNEL_DISABLED";
	case Errors::CHANNEL_BUSY:
		return "CHANNEL_BUSY";
	default:
		return "FIXME: Error ("+boost::lexical_cast<std::string>(e)+") not translated at "+std::string(__FILE__);
	}
}

}

