#include <LogIt.h>

#include <Hdlc/BackendFactory.h>

#include <Sca/Defs.h>
#include <Sca/Sca.h>

#include <boost/lexical_cast.hpp>

#include <ScaCommon/except.h>
#include <stdexcept>
#include <vector>
#include <fstream>
#include <iterator>


using boost::lexical_cast;
namespace Sca
{

/*
* GPIO COMMANDS
*/

/*
* LOW LEVEL
*/

// This function is used to getReadCommandFromRegister.
// Instead of having a separate command for each register, the user will use more general commands which in turn use this function
uint32_t Sca::Gpio::getReadCommandFromRegister(uint32_t reg)
{
	switch(reg)
	{
	case ::Sca::Constants::GpioRegisters::DataOut: 		return ::Sca::Constants::Commands::GPIO_R_DATAOUT; break;
	case ::Sca::Constants::GpioRegisters::DataIn : 		return ::Sca::Constants::Commands::GPIO_R_DATAIN; break;
	case ::Sca::Constants::GpioRegisters::Direction : 	return ::Sca::Constants::Commands::GPIO_R_DIRECTION; break;
	case ::Sca::Constants::GpioRegisters::InterruptSelect :	return ::Sca::Constants::Commands::GPIO_R_INTSEL; break;
	case ::Sca::Constants::GpioRegisters::InterruptTrigger :return ::Sca::Constants::Commands::GPIO_R_INTTRIG; break;
	case ::Sca::Constants::GpioRegisters::EdgeSelect :	return ::Sca::Constants::Commands::GPIO_R_EDGESEL; break;
	case ::Sca::Constants::GpioFlags::InterruptEnable :	return ::Sca::Constants::Commands::GPIO_R_INTENABLE; break;
	case ::Sca::Constants::GpioFlags::ClockSelect : 	return ::Sca::Constants::Commands::GPIO_R_CLKSEL; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
// This function is used to getWriteCommandFromRegister.
// Instead of having a separate command for each register, the user will use more general commands which in turn use this function
uint32_t Sca::Gpio::getWriteCommandFromRegister(uint32_t reg)
{
	switch(reg)
	{
	case ::Sca::Constants::GpioRegisters::DataOut: 		return ::Sca::Constants::Commands::GPIO_W_DATAOUT; break;
	case ::Sca::Constants::GpioRegisters::Direction : 	return ::Sca::Constants::Commands::GPIO_W_DIRECTION; break;
	case ::Sca::Constants::GpioRegisters::InterruptSelect :	return ::Sca::Constants::Commands::GPIO_W_INTSEL; break;
	case ::Sca::Constants::GpioRegisters::InterruptTrigger :return ::Sca::Constants::Commands::GPIO_W_INTTRIG; break;
	case ::Sca::Constants::GpioRegisters::EdgeSelect :	return ::Sca::Constants::Commands::GPIO_W_EDGESEL; break;
	case ::Sca::Constants::GpioFlags::InterruptEnable :	return ::Sca::Constants::Commands::GPIO_W_INTENABLE; break;
	case ::Sca::Constants::GpioFlags::ClockSelect : 	return ::Sca::Constants::Commands::GPIO_W_CLKSEL; break;
	default: throw std::runtime_error("[GPIO] invalid register input"); break;
	}
}
//This is the prototype method for sending a WRITE command
void Sca::Gpio::sendWriteDataToRegister(uint32_t data, uint32_t reg)
{
	boost::lock_guard<boost::mutex> lock( m_single_access_mutex_GPIO );
	Request request(
			::Sca::Constants::ChannelIds::GPIO,
			getWriteCommandFromRegister(reg),
			// NOTE: the order below has been validated by Piotr with the SCA digital daughterboard 13-07-2017
			{ (uint8_t) (data >> 16),  
			  (uint8_t) (data >> 24),  
			  (uint8_t) (data >>  0),  
			  (uint8_t) (data >>  8)   
			}
			 );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	throwIfScaReplyError(reply);
}
//This is the prototype method for sending a READ command
uint32_t Sca::Gpio::sendReadRegister(uint32_t reg)
{
	boost::lock_guard<boost::mutex> lock( m_single_access_mutex_GPIO );
	Request request(
			::Sca::Constants::ChannelIds::GPIO,
			getReadCommandFromRegister(reg),
			{}
			 );
	Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
	
	throwIfScaReplyError(reply);
	// NOTE: the order below has been validated by Piotr with the SCA digital daughterboard 13-07-2017
	return reply[5]<<24 | reply[4]<<16 | reply[7]<<8 | reply[6];
}
/*
*	USER LEVEL
*/
//The user can call:
// >my_sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataOut)
//to read DataOut
// Or the user can call:
// >my_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut, 0xC0CAC01A)
//to write the 32-bit word C0CAC01A to register DataOut
uint32_t Sca::Gpio::getRegister(uint32_t reg)
{
	return sendReadRegister(reg);
}
void Sca::Gpio::setRegister(uint32_t reg, uint32_t val)
{
	LOG(Log::INF) << " reg= " << std::hex << reg << " val= " << val;
	return sendWriteDataToRegister(val, reg);
}
bool Sca::Gpio::getRegisterBitValue(uint32_t reg, int pin)
{
	if(reg==::Sca::Constants::GpioFlags::InterruptEnable || reg==::Sca::Constants::GpioFlags::ClockSelect)
		throw_runtime_error_with_origin("Cannot use this command on flag register");

	if(pin<0 || pin>31) throw_runtime_error_with_origin("Invalid Pin number");

	return (  getRegister(reg)  ) & (1<<pin);
}

void Sca::Gpio::setRegisterBitToValue(uint32_t reg, int pin, bool val)
{
	if(reg==::Sca::Constants::GpioFlags::InterruptEnable || reg==::Sca::Constants::GpioFlags::ClockSelect)
		throw_runtime_error_with_origin("Cannot use this command on flag register");

	if(pin<0 || pin>31) throw_runtime_error_with_origin("Invalid Pin number");
	uint32_t my_register = getRegister(reg);
	my_register &= ~(1<<pin); //clears the pin-th pin
	if(val)
		my_register |= 1<<pin; //if we were to set it to 1, now it will be set to 1
	setRegister(reg,my_register);
}
std::vector<bool> Sca::Gpio::getRegisterRangeValue(uint32_t reg, int startPin, int endPin)
{
	if(reg==::Sca::Constants::GpioFlags::InterruptEnable || reg==::Sca::Constants::GpioFlags::ClockSelect)
		throw_runtime_error_with_origin("Cannot use ranged command on flag register");

	if(startPin<0 || startPin>31 || endPin<0 || endPin>31) throw_runtime_error_with_origin("Invalid Pin number");
	if(startPin>endPin) throw_runtime_error_with_origin("startPin > endPin not allowed");

	uint32_t my_register = getRegister(reg);
	std::vector<bool> my_values;

	for(int pin=startPin;pin<=endPin;pin++)
	{
		my_values.push_back(  my_register & (1<<pin)  );
	}
	return my_values;	
}
void Sca::Gpio::setRegisterRangeToValue(uint32_t reg, int startPin, int endPin, bool val)
{
	if(reg==::Sca::Constants::GpioFlags::InterruptEnable || reg==::Sca::Constants::GpioFlags::ClockSelect)
		throw_runtime_error_with_origin("Cannot use ranged command on flag register");	

	if(startPin<0 || startPin>31 || endPin<0 || endPin>31) throw_runtime_error_with_origin("Invalid Pin number");
	if(startPin>endPin) throw_runtime_error_with_origin("startPin > endPin not allowed");

	uint32_t my_register = getRegister(reg);
	for(int pin=startPin;pin<=endPin;pin++)//the commands inside the IF are copied from function setRegisterBitValue
	{
		my_register &= ~(1<<pin); //clears the pin-th pin
		if(val)
			my_register |= 1<<pin; //if we were to set it to 1, now it will be set to 1
	}
	setRegister(reg,my_register);
}

}
