#!/bin/sh

BUILD=build

echo Removing old $BUILD/ directory.
rm -Rf $BUILD
mkdir $BUILD
cd $BUILD
cmake ../ -DSTANDALONE_SCA_SOFTWARE=1 -DHAVE_NETIO=1 -DSTANDALONE_WITH_EVENT_RECORDER=ON -DCMAKE_BUILD_TYPE=Debug
make -j `nproc`
cd ..
echo "Build was made in directory $BUILD/."

exit 0

