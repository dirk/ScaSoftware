#include <iostream>
#include <stdlib.h>

#include <boost/program_options.hpp>
#include <boost/thread.hpp>

#include <felixbase/client.hpp>
#include <netio.hpp>
#include <hdlc.hpp>

#include <ScaSimulator/HdlcBackend.h>
#include <LogIt.h>

typedef uint8_t ElinkId;

const unsigned int NETIO_FRAME_HDLC_START_OFFSET = 8;  // HDLC frame shall be placed starting from this index
const unsigned int HDLC_FRAME_PAYLOAD_OFFSET = 2;   // HDLC payload offset in HDLC frame
const unsigned int HDLC_FRAME_TRAILER_SIZE = 2;   // HDLC payload offset in HDLC frame

struct Config
{
    Log::LOG_LEVEL       logLevel;
    uint16_t             requestsPort;
    uint16_t             repliesPort;
    ElinkId              elinkId;
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
            ("help", "show help")
            ("trace_level,t",   value<std::string>(&logLevelStr)->default_value("INF"),               "Trace level, one of: ERR,WRN,INF,DBG,TRC")
            ("requests_port,x", value<uint16_t>(&config.requestsPort)->default_value(29000),          "TCP/IP port on which netio will listen for requests")
            ("replies_port,y",  value<uint16_t>(&config.repliesPort)->default_value(29001),           "TCP/IP port to which netio will publish the replies")
            ("elink_id,e",      value<ElinkId>(&config.elinkId)->default_value(0x01),                 "Elink-id at which this simulated SCA listens")
            ;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    if (! Log::logLevelFromString( logLevelStr, config.logLevel ) )
    {
        std::cout << "Log level not recognized: '" << logLevelStr << "'" << std::endl;
        exit(1);
    }

    return config;
}

int main(int argc, char* argv[] )
{
    Config config = parseProgramOptions( argc, argv);
	Log::initializeLogging( config.logLevel );
	LOG(Log::INF) << "initialized logging";

	netio::context context("posix");

	boost::thread netio_thread( [&context](){
		context.event_loop()->run_forever();

	});

	/* NOTE for the id. It doesn't matter as long as we keep one simulated SCA in this program.
	 * It doesn't get exposed externally either.
	 */
	ScaSimulator::HdlcBackend simulator ("1");

	netio::publish_socket reply_socket( &context, config.repliesPort );
	netio::tag publish_tag ( config.elinkId );

	uint64_t seqnr = 0;

	// This is a "lambda" type implementation which fits expected std::function handler
	Hdlc::Backend::ReceiveCallBack receiveCallback ( [&reply_socket,&publish_tag,&seqnr](const Hdlc::Payload& payload)
        {
            // HDLC framing - Paris

            if (seqnr >= 7) seqnr = 0; else seqnr++;

            int addr = 0;

            std::vector<uint8_t> netioFrame (payload.size() + 12);

            felix::hdlc::encode_hdlc_msg_header(&netioFrame[8], addr, seqnr);
            std::copy(payload.cbegin(), payload.cend(), &netioFrame[10]);
            felix::hdlc::encode_hdlc_trailer(&netioFrame[8+2+payload.size()], &netioFrame[8+2], payload.size() + 2);

            netio::message msg( &netioFrame[0], netioFrame.size() );
            reply_socket.publish( publish_tag, msg );
            LOG(Log::TRC) << "Published reply( transaction id " << (int)payload[0] << ")";

        }
	);
	simulator.subscribeReceiver( receiveCallback );

	netio::low_latency_recv_socket socket(
	        &context,
	        config.requestsPort,
	        [&simulator,&config](netio::endpoint &ep, netio::message& msg)
        {
            LOG(Log::TRC) << "Received netio message, size=" << msg.size() << ", number of fragments: " << msg.num_fragments();
            if (msg.size() < 24) // TODO?
            {
                LOG(Log::INF) << "Obtained message smaller than expected for SCA, ignoring it. (It might be a HDLC control frame)";
                return;
            }

            std::vector<uint8_t> netioFrame (msg.data_copy());

            felix::base::ToFELIXHeader header;
            std::copy(
                    &netioFrame[0],
                    &netioFrame[0] + sizeof header,
                    reinterpret_cast<uint8_t*>(&header));

            if (header.elinkid != config.elinkId)
            {
                LOG(Log::TRC) << "Received a message destined for elink " << header.elinkid << ", ignoring it, ours is " << (unsigned int)config.elinkId;
                return;
            }

            uint8_t* hdlcPayloadPtr = &netioFrame[ sizeof header + HDLC_FRAME_PAYLOAD_OFFSET ];

            Hdlc::Payload payload(
                    hdlcPayloadPtr,
                    netioFrame.size() - sizeof header - HDLC_FRAME_PAYLOAD_OFFSET - HDLC_FRAME_TRAILER_SIZE);

            LOG(Log::TRC) << "obtained frame: " << payload.toString();

            simulator.send( payload );

        }
	);

	LOG(Log::INF) << "I think that I'm ready to accept connections from SCA-SW through netio. "
	        "I listen on port " << config.requestsPort << " for SCA requests "
	        "and I publish replies on port " << config.repliesPort <<
	        " and my elink is: " << (unsigned int)config.elinkId;
	while(1)
	{
		usleep(100000);
	}

}

