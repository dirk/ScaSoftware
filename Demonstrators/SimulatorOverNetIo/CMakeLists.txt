link_directories(/usr/lib64/boost148)
add_executable(simulator_netio
  main.cpp
  ${DEMONSTRATORS_COMMON_OBJS}
  )

target_link_libraries(simulator_netio ${DEMONSTRATORS_COMMON_LIBS} )
