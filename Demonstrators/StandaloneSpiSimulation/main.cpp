/*
 * main.cpp
 *
 *  Created on: October 21, 2016
 *      Author: Paris Moschovakos
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>

using std::vector;

struct Config
{
    std::string          address;
    Log::LOG_LEVEL       logLevel;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  "SCA address, i.e. sca-simulator://1 or simple-netio://direct/pcatlnswfelix01.cern.ch/12340/12345/BF" )
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    if (! Log::logLevelFromString( logLevelStr, config.logLevel ) )
    {
	std::cout << "Log level not recognized: '" << logLevelStr << "'" << std::endl;
	exit(1);
    }

    return config;
}

int main (int argc, char* argv[])
{
	Config config = parseProgramOptions(argc, argv);
	srand(time(0));
	Log::initializeLogging( config.logLevel );

	Sca::Sca sca ( config.address ) ;

	while (1)
	{
		try
		{

			/* Configure a VMM example */
			vector<uint8_t> vmmConfigurationData
			{//is SOP 0x00
				0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x04,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10, 0x00, 0x00, 0x10,
				0x00, 0x00, 0x00, 0x00, 0x02, 0x80, 0x34, 0x84, 0xc0, 0x10, 0x22, 0x20
				// last bit is sp (polarity) and it is the last bit to be written to VMM
			};

			//sca.setChannelEnabled( ::Sca::Constants::ChannelIds::SPI, true );

// Step 1
			sca.spi().setTransmissionBaudRate( 10000000 ); // 20MHz (SCA max speed)
			auto rate = sca.spi().getTransmissionBaudRate();
			LOG(Log::INF) << "Baud rate set at: " << (int)rate << "Hz" ;

// Step 2
			sca.spi().setTransmisionBitLength( 96 );
			auto trLen2 = sca.spi().getTransmisionBitLength();
			LOG(Log::INF) << "Transmission length set at: " << (int)trLen2 << "bits" ;

// Step 3: Set ENA to low (0)
			uint32_t directionOut = 0x00000400;
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,directionOut);
			uint32_t gpioOutputValue = 0x00000000; //0x00000400 = FFFFFBFF
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,gpioOutputValue);

// Step 4: Set the configuration register - Some of the following are fucked up like the IE (not the internet explorer).
			LOG(Log::INF) << "Setting the control register..." ;
			sca.spi().setInterruptEnable(true);
			sca.spi().setSsMode(false);
			//sca.spi().resetSelectedSlave();	// Setting CS on line 0, where the VMM is, to high
			//Now done inside spi().write
			sca.spi().setTxEdge(false);
			sca.spi().setLsbToMsb(false);
			sca.spi().setInvSclk(false);
			LOG(Log::INF) << "Starting VMM configuration..." ;
			for (int i = 0; i < 1; i++)
			{

// Step 5
				LOG(Log::INF) << "Configuring VMM: " << i ;
				sca.spi().writeSlave( i, vmmConfigurationData );

// Step 6: Set CS to high (1)
				// sca.spi().resetSelectedSlave(); //Now done inside spi().write
			}

// Step 7: Set ENA to high (1)
			gpioOutputValue = 0x00000400;
			sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,gpioOutputValue);

// Step 8
//			sca.setChannelEnabled( ::Sca::Constants::ChannelIds::SPI, false );
			std::cout << "---------------------------" << std::endl ;
			LOG(Log::INF) << "VMM configuration done! " ;
			std::cout << "---------------------------" << std::endl ;
			usleep(60000000);
/*
 * 			Where is the SCLK? - Toggle the CS
 */

//			for (int i = 0; i < 1; i++)
//			{
//				LOG(Log::INF) << "~~~~~~~~~~~~~~~~~~~Configuring: Setting CS to 0 in order to write a configuration";
//				sca.spi().writeSlave( i, vmmConfigurationData );
//				sca.spi().setSelectedSlave(5);
//				//usleep(1000000);
//			}
//
//			LOG(Log::INF) << "~~~~~~~~~~~~~~~~~~~Finalizing: Finished configuring, bring it back to high";
//			usleep(1000000);
/*
 * 				Control Register Tests
 * */
			// Transmission bit length test
//			sca.spi().setTransmisionBitLength( 96 );
//			auto trLen2 = sca.spi().getTransmisionBitLength();
//			LOG(Log::INF) << "Transmission length set at: " << (int)trLen2 << "bits" ;

			// Invert SCLK level during inactivity test
//			sca.spi().setInvSclk(false);
//			bool reply1 = sca.spi().getInvSclk();
//			LOG(Log::INF) << "Is SPI clock idle high?: " << reply1;

			// Manual SPI Control bit test - Go/Busy
//			sca.spi().setGoFlag(true);
//			bool reply2 = sca.spi().getBusyFlag();
//			LOG(Log::INF) << "Is SPI Busy?: " << reply2;

			// Define SCLK sampling edge of the MISO input line test
//			sca.spi().setRxEdge(true);
//			bool reply3 = sca.spi().getRxEdge();
//			LOG(Log::INF) << "Is MISO signal sampled on the falling edge?: " << reply3;

			// Define SCLK sampling edge of the MOSI output line test
//			sca.spi().setTxEdge(true);
//			bool reply4 = sca.spi().getTxEdge();
//			LOG(Log::INF) << "Is MOSI signal changed on the falling edge?: " << reply4;

/*
 * 				Configuring the SPI bus test
 * */
			/* 1. Test for setting the Baud rate */
//			auto freq = sca.spi().getTransmissionBaudRate();
//			LOG(Log::INF) << "Baud Rate: " << freq << " Hz";
//			sca.spi().setTransmissionBaudRate( 20000000 ); /* allowed range 305Hz - 20MHz */
//			auto freq2 = sca.spi().getTransmissionBaudRate();
//			LOG(Log::INF) << "New Baud Rate: " << freq2 << " Hz";

			/* 2. Test for setting the SPI mode */


			/* 3. General test SPI write multiple chunks */
//			vector<uint8_t> spiPayload
//			{
//				0x01, 0x02, 0xA3, 0x04, 0x05, 0x06, 0x07, 0x08,
//				0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
//				0xA5, 0xA2, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
//				0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x11
//			};

//			sca.setChannelEnabled( ::Sca::Constants::ChannelIds::SPI, true );
//			sca.spi().writeSlave( 2, spiPayload );
//			LOG(Log::INF) << "SPI payload was sent";
//			LOG(Log::INF) << "Transmission length: " << (int)trLen2 << "bits" ;
//
//			sca.setChannelEnabled( ::Sca::Constants::ChannelIds::SPI, false );

		}
		catch (std::exception &e)
		{
			LOG(Log::ERR) << "exception " << e.what();
		}

		usleep(1000000);
	}






}
