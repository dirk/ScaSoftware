/*
 * PingSca.cpp
 *
 *  Created on: Aug 31, 2017
 *      Author: pnikiel
 *  Note: the original idea for this program is of Paris Moschovakos
 */

#include <iostream>
#include <sstream>
#include <boost/program_options.hpp>
#include <Sca/Sca.h>

template<typename T>
std::string toStringInHex (T v)
{
	std::stringstream ss;
	ss.width(2);
	ss.fill('0');
	ss << std::hex << v;
	return ss.str();
}

struct Config
{
    std::string          hostname;
    unsigned int		 from;
    unsigned int		 to;

};
Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    Config config;
    options_description options;
    options.add_options()
	("help", "show help")
	("hostname",      value<std::string>(&config.hostname)->default_value("localhost"),  "hostname where felixcore is running" )
	("from",          value<unsigned int>(&config.from)->default_value(0),               "elinks range lower bound")
	("to",            value<unsigned int>(&config.to)->default_value(0xff),               "elinks range upper bound")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
		std::cout << options << std::endl;
		exit(1);
    }
    return config;
}

int main (int argc, char* argv[])
{
	Config config = parseProgramOptions(argc,argv);

	const unsigned int numElinks = config.to - config.from + 1;

	struct ScaInfo
	{
		ScaInfo () { present = false; id = 0x0; }
		bool present;
		uint32_t id;
	};

	std::vector<ScaInfo> foundSca (numElinks, ScaInfo());
	std::cout << "-- scanning begun; it will take ~5 minutes. Summary results will be printed then." << std::endl;
	for (unsigned int elink=config.from; elink<=config.to; ++elink)
	{
		const std::string address = "simple-netio://direct/" + config.hostname + "/12340/12345/" + toStringInHex (elink);
		try
		{
		    Sca::Sca sca  (address);   // this throws when SCA's not replying at this address
		    foundSca[elink-config.from].present = true;
		    foundSca[elink-config.from].id = sca.getChipId();
		    std::cout << "!! found sca elink=" << toStringInHex(elink) << std::endl;
		}
		catch (...)
		{
		  // do nothing when SCA not found at this address

		}
	}
	std::cout << "-- scanning results for hostname " << config.hostname << " -- " << std::endl;
	for (unsigned int elink=config.from; elink <= config.to; ++elink)
		if (foundSca[elink-config.from].present)
			std::cout << "elink " << toStringInHex(elink) << " found SCA, serial=" << foundSca[elink-config.from].id << std::endl;

}


