link_directories( ${DEMONSTRATORS_COMMON_LIBDIRS} )

add_executable(standalone_i2c 
  main.cpp
  ${DEMONSTRATORS_COMMON_OBJS} 
  )

target_link_libraries(standalone_i2c ${DEMONSTRATORS_COMMON_LIBS} )

