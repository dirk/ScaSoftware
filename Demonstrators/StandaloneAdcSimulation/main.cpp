/*
 * main.cpp
 *
 *  Created on: May 3, 2016
 *      Author: pnikiel
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>

#ifdef WITH_EVENT_RECORDER
#include <VeryHipEvents/EventRecorder.h>
using VeryHipEvents::Recorder;
#endif

struct Config
{
    std::string          address;
    double               frequency;
    Log::LOG_LEVEL       logLevel;
    unsigned int         iterations;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	        ("help", "show help")
	        ("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  "SCA address, i.e. sca-simulator://1 or simple-netio://127.0.0.1:10000" )
	        ("frequency,f",   value<double>(&config.frequency)->default_value(1000.0/*Hz*/),            "Frequency of ADC polling")
	        ("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	        ("iterations,i" , value<unsigned int>(&config.iterations)->
	                default_value(std::numeric_limits<int>::max()),                                     "Number of repetitions" )
	        ;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
        std::cout << options << std::endl;
        exit(1);
    }
    if (! Log::logLevelFromString( logLevelStr, config.logLevel ) )
    {
        std::cout << "Log level not recognized: '" << logLevelStr << "'" << std::endl;
        exit(1);
    }

    return config;
}

int main (int argc, char* argv[])
{
    Config config = parseProgramOptions( argc, argv);
    srand(time(0));
    Log::initializeLogging( config.logLevel );

    #ifdef WITH_EVENT_RECORDER
    Recorder::initialize (1E6, 1E8 );
    #endif // WITH_EVENT_RECORDER

    Sca::Sca sca ( config.address ) ;
    LOG(Log::INF) << "The chip id is: " << std::hex << sca.getChipId();

    unsigned int i=0;
    while (i < config.iterations)
    {
        try
        {
            for (int i=0; i<32; i++)
            {
                auto value = sca.adc().convertChannel(i);


                LOG(Log::INF) << "ch " <<i << " obtained value " << value;

            }
        }
        catch (std::exception &e)
        {
            LOG(Log::ERR) << "exception " << e.what();
        }

        usleep(1000000.0 / config.frequency );
        ++i;
    }

    #ifdef WITH_EVENT_RECORDER
    Recorder::dump("dump.xml");
    #endif // WITH_EVENT_RECORDER




}
