/*
 * main.cpp
 *
 *  Created on: February 3, 2017
 *      Author: aikoulou, based on pnikiel
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>

#include <boost/program_options.hpp>
struct Config
{
    std::string          address;
    double               frequency;
    Log::LOG_LEVEL       logLevel;
    
};
Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  "SCA address, i.e. sca-simulator://1 or simple-netio://127.0.0.1:10000" )
	("frequency,f",   value<double>(&config.frequency)->default_value(1.0/*Hz*/),            "Frequency of polling")
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    if (! Log::logLevelFromString( logLevelStr, config.logLevel ) )
    {
	std::cout << "Log level not recognized: '" << logLevelStr << "'" << std::endl;
	exit(1);
    }
    
    return config;
}
int main (int argc, char* argv[])
{
	Config config = parseProgramOptions( argc, argv);
	srand(time(0));
	Log::initializeLogging( config.logLevel );
	Sca::Sca sca ( config.address ) ;

LOG(Log::INF) << "[JTAG] Testing chain";

sca.jtag().setFrequency_MHz(0.5);
LOG(Log::INF) << "[JTAG] freq set at   = 0.5";
LOG(Log::INF) << "[JTAG] freq readback = "<<sca.jtag().getFrequency_MHz();

//sca.jtag().scanChain();
sca.jtag().getFpgaId();
/*
//test write/read TMS

uint32_t s;
for(int i=0;i<4;i++)
{
s=0xFFFFFFFF;
sca.jtag().writeTDO(i,s);
LOG(Log::INF) << "[JTAG] writeTDO "<<i <<" with "<<std::hex<<s<< " read "<< sca.jtag().readTDI(i);
s=0x0;
sca.jtag().writeTDO(i,s);
LOG(Log::INF) << "[JTAG] writeTDO "<<i <<" with "<<std::hex<<s<< " read "<< sca.jtag().readTDI(i);
s=0xFFFF0000;
sca.jtag().writeTDO(i,s);
LOG(Log::INF) << "[JTAG] writeTDO "<<i <<" with "<<std::hex<<s<< " read "<< sca.jtag().readTDI(i);
s=0x0000FFFF;
sca.jtag().writeTDO(i,s);
LOG(Log::INF) << "[JTAG] writeTDO "<<i <<" with "<<std::hex<<s<< " read "<< sca.jtag().readTDI(i);
s=0xABCD0123;
sca.jtag().writeTDO(i,s);
LOG(Log::INF) << "[JTAG] writeTDO "<<i <<" with "<<std::hex<<s<< " read "<< sca.jtag().readTDI(i);
}
*/

//test trans length
/*
LOG(Log::INF) << "[JTAG] transmission length "<< sca.jtag().getTransmissionLength_Bits();

for(int i=1;i<130;i++)//crashes with runtime error when not in range (as expected!)
{
sca.jtag().setTransmissionLength_Bits(i);
LOG(Log::INF) << "[JTAG] trans len set "<<i << " read "<< sca.jtag().getTransmissionLength_Bits();

}
*/


//sca.jtag().scanChain();
//test freq div
/*
uint16_t s;
for(int i=65000;i<66000;i++)
{
sca.jtag().setFrequencyDivider(i);
LOG(Log::INF) << "[JTAG] freq div set "<<i << " read "<< sca.jtag().getFrequencyDivider();
}
*/
//getTransmissionLength_Bits(1);




//sca.jtag().getFpgaId();
//expect something like X3691093

//test CTRL
/*
LOG(Log::INF) << "[JTAG] CTRL = "<<std::hex<< sca.jtag().readCTRL();

s=0x00FF;sca.jtag().writeCTRL(s);LOG(Log::INF) << "[JTAG] wrote="<< std::hex << s  << "\t : "<<std::hex<< sca.jtag().readCTRL();
s=0xFF00;sca.jtag().writeCTRL(s);LOG(Log::INF) << "[JTAG] wrote="<< std::hex << s  << "\t : "<<std::hex<< sca.jtag().readCTRL();
s=0xFFFF;sca.jtag().writeCTRL(s);LOG(Log::INF) << "[JTAG] wrote="<< std::hex << s  << "\t : "<<std::hex<< sca.jtag().readCTRL();
s=0x0000;sca.jtag().writeCTRL(s);LOG(Log::INF) << "[JTAG] wrote="<< std::hex << s  << "\t : "<<std::hex<< sca.jtag().readCTRL();
*/

/*
LOG(Log::INF) << "[JTAG] ======== test fpga ID ==============";

//LOG(Log::INF) << "[JTAG] " << 
sca.jtag().getFpgaId();
sca.jtag().getFpgaFuseDna();
sca.jtag().getFpgaXscDna();


exit(0);

LOG(Log::INF) << "[JTAG] ======================";
sca.jtag().setFrequency_MHz(1.0);
LOG(Log::INF) << "[JTAG] freq readback = " << sca.jtag().getFrequency_MHz();
LOG(Log::INF) << "[JTAG] ======================";
LOG(Log::INF) << "[JTAG] ======================";
	sca.jtag().programFPGAwithFile("/home/sca_developers/binFiles/led_counter.bit");
	LOG(Log::INF) << "[JTAG] end of standalone JTAG ";
*/
}
