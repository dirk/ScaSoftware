/*
 * DAC.cpp
 *
 *  Created on: November 7, 2016
 *      Author: aikoulou, based on pnikiel
 */
#include <Sca/Defs.h>
#include <ScaSimulator/DAC.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{
DAC::DAC(unsigned char channelId, HdlcBackend *myBackend) :
		ScaChannel(channelId, myBackend),
		m_DAC_A(0x00),
		m_DAC_B(0x00),
		m_DAC_C(0x00),
		m_DAC_D(0x00)
{
}
DAC::~DAC()
{}

bool DAC::isDacReadCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::DAC_R_A : return true; break;
		case Sca::Constants::Commands::DAC_R_B : return true; break;
		case Sca::Constants::Commands::DAC_R_C : return true; break;
		case Sca::Constants::Commands::DAC_R_D : return true; break;
		default : return false;break;
	}//switch
}//isDacReadCommand
bool DAC::isDacWriteCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::DAC_W_A : return true; break;
		case Sca::Constants::Commands::DAC_W_B : return true; break;
		case Sca::Constants::Commands::DAC_W_C : return true; break;
		case Sca::Constants::Commands::DAC_W_D : return true; break;
		default : return false;break;
	}//switch
}//isDacWriteCommand


uint8_t DAC::getRegisterFromCommand(uint32_t regCmd)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::DAC_R_A : return m_DAC_A; break;
		case Sca::Constants::Commands::DAC_R_B : return m_DAC_B; break;
		case Sca::Constants::Commands::DAC_R_C : return m_DAC_C; break;
		case Sca::Constants::Commands::DAC_R_D : return m_DAC_D; break;
		default : return -1;break;
	}
}//getRegisterFromCommand

void DAC::setRegisterFromCommand(uint32_t regCmd,uint8_t data)
{
	switch(regCmd)
	{
		case Sca::Constants::Commands::DAC_W_A :  m_DAC_A = data; break;
		case Sca::Constants::Commands::DAC_W_B :  m_DAC_B = data; break;
		case Sca::Constants::Commands::DAC_W_C :  m_DAC_C = data; break;
		case Sca::Constants::Commands::DAC_W_D :  m_DAC_D = data; break;
		default : break;
	}
}//setRegisterFromCommand


void DAC::sendReadReply(const Sca::Request& request,uint32_t regCmd)
    {
	uint8_t regToSend = getRegisterFromCommand(regCmd);

	Sca::Reply reply (
		Sca::Constants::ChannelIds::DAC,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {0,0,0,
				(uint8_t) regToSend
				});

    	unsigned int replyDelay = 1 + rand()%2;
        std::cout << "The chosen t=" << replyDelay << std::endl;
        boost::chrono::milliseconds processingTime (replyDelay);
        this->sendReply( request, reply, processingTime );
    	this->keepBusyFor( processingTime );
}//sendReadReply

void DAC::sendWriteReply(const Sca::Request& request,uint32_t regCmd, uint8_t data)
    {

	Sca::Reply reply (
		Sca::Constants::ChannelIds::DAC,//channel id
		/*err*/ 0x00,
		/*data = list of 4 bytes*/ {0,0,0,0});

    	unsigned int replyDelay = 1 + rand()%2;
        std::cout << "The chosen t=" << replyDelay << std::endl;
        boost::chrono::milliseconds processingTime (replyDelay);
        this->sendReply( request, reply, processingTime );
    	this->keepBusyFor( processingTime );

	setRegisterFromCommand(regCmd,data);

}//sendReadReply


void DAC::onReceive(const Sca::Request &request)
{
	uint32_t cmdOrError = request.command();

	if(isDacReadCommand(cmdOrError))
		sendReadReply(request, cmdOrError);
	else if(isDacWriteCommand(cmdOrError))
		sendWriteReply(request, cmdOrError, request[7]);
}

}
