/*
 * ScaChannel.cpp
 *
 *  Created on: May 5, 2016
 *      Author: pnikiel
 */

#include <ScaSimulator/HdlcBackend.h>
#include <ScaSimulator/ScaChannel.h>

namespace ScaSimulator
{


/**
 * @param reply its transaction id will be modified to match the originating transaction id
 */
    void ScaChannel::sendReply(
		const Sca::Request& request,
		Sca::Reply& reply,
		boost::chrono::microseconds t)
    {
		reply.assignTransactionId( request.transactionId() );
		m_myBackend->storeReply( reply, t );
    }

    void ScaChannel::sendReplyLaterKeepBusyFor (
	const Sca::Request & request,
	Sca::Reply & reply,
	double delaySeconds )
    {
	boost::chrono::microseconds delayUs ( (unsigned int)(1E6 * delaySeconds));
	m_busyUntil = boost::chrono::steady_clock::now() + delayUs; 
	reply.assignTransactionId( request.transactionId() );
	m_myBackend->storeReply( reply, delayUs );
    }

}
