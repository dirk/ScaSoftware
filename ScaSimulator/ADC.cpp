/*
 * ADC.cpp
 *
 *  Created on: May 4, 2016
 *      Author: pnikiel
 */

#include <ScaSimulator/ADC.h>
#include <ScaSimulator/HdlcBackend.h>

#include <Sca/Defs.h>

#include <Sca/Request.h>
#include <Sca/Reply.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{

ADC::ADC(unsigned char channelId, HdlcBackend *myBackend) :
		ScaChannel(channelId, myBackend)
{
}

ADC::~ADC()
{
}

    void ADC::onReceive(const Sca::Request &request)
    {
	
	using Sca::Constants::Commands;

	switch( request.command() )
	{
	case Commands::ADC_GO:
	{
	    uint16_t adcValue = rand() & 0x0fff ; // simulate noise, the and mask ensures 12bit value (the ADC is 12 bits)
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { 0, 0, (uint8_t)(adcValue&0xff), (uint8_t)((adcValue&0x0f00)>>8) } );
	    //NOTE: SCA manual v8.0 says for the new ADC the worst case ADC conversion time is 150us. so we choose a random between 100us and 150us
	    unsigned int conversionDelay = 100 + rand()%50;
	    this->sendReplyLaterKeepBusyFor( request, reply, double(conversionDelay)/1E6 );
	}
	break;
	case Commands::ADC_W_INSEL:
	{
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
	    this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );
	}
	break;
	case Commands::ADC_R_GAIN:
	{
		// TODO: pnikiel: it is not yet known how the gain command is to be interpreted, so right now we consider empty data...
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
	    this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );
	}
	break;
	case Commands::ADC_W_GAIN:
	{
		// TODO: pnikiel: it is not yet known how the gain command is to be interpreted, so right now we consider empty data...
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
	    this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );
	}
	break;
	case Commands::CTRL_R_ID:
	{
	    uint32_t scaId = backend()->getScaId();
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { uint8_t(scaId>>16), 0, uint8_t(scaId&0xff), uint8_t((scaId>>8)&0xff) } );
        this->sendReplyLaterKeepBusyFor( request, reply, 0.000001 );
	}
	break;
	}


    }



}
