/*
 * I2C.cpp
 *
 *  Created on: May 26, 2017
 *      Author: Paris Moschovakos
 */


#include <ScaSimulator/I2C.h>

#include <Sca/Defs.h>

#include <stdlib.h>

#include <iostream>

namespace ScaSimulator
{

I2C::I2C(unsigned char channelId, HdlcBackend *myBackend) :
		ScaChannel(channelId, myBackend)
{
}

I2C::~I2C()
{
}

    void I2C::onReceive(const Sca::Request &request)
    {
	using Sca::Constants::Commands;

	switch( request.command() )
	{
    case Commands::I2C_R_CTRL:
    {
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { 0, (uint8_t)m_divider , 0, 0} );
        unsigned int delay = 1 + rand()%2;
        std::cout << "The chosen t=" << delay << std::endl;
        this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
    }
    break;
    case Commands::I2C_W_CTRL:
    {
        m_divider = 10;
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ {} );
        unsigned int delay = 1 + rand()%2;
        this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
    }
    break;
    case Commands::I2C_R_STR:
    {
        Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { 0, (uint8_t)m_divider, 0, 0} );
        unsigned int delay = 1 + rand()%2;
        this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
    }
    break;
	case Commands::I2C_M_7B_W:
	{
	    Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { 0, 4, 0, 0 } );
	    unsigned int delay = 1 + rand()%2;
	    this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
	}
	break;
	case Commands::I2C_W_DATA0:
		{
			uint32_t i2cValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(i2cValue), (uint8_t)((i2cValue)>>8),  (uint8_t)((i2cValue)>>16), (uint8_t)((i2cValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;
		    this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
		}
	break;
	case Commands::I2C_W_DATA1:
		{
			uint32_t i2cValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(i2cValue), (uint8_t)((i2cValue)>>8),  (uint8_t)((i2cValue)>>16), (uint8_t)((i2cValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;
		    this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
		}
	break;
	case Commands::I2C_W_DATA2:
		{
			uint32_t i2cValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(i2cValue), (uint8_t)((i2cValue)>>8),  (uint8_t)((i2cValue)>>16), (uint8_t)((i2cValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;
		    this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
		}
	break;
	case Commands::I2C_W_DATA3:
		{
			uint32_t i2cValue = rand();
			Sca::Reply reply (request.channelId(), /*err*/ 0x00, /*data*/ { (uint8_t)(i2cValue), (uint8_t)((i2cValue)>>8),  (uint8_t)((i2cValue)>>16), (uint8_t)((i2cValue)>>24) } );
		    unsigned int delay = 1 + rand()%2;
		    this->sendReplyLaterKeepBusyFor( request, reply, double(delay)/1E6 );
		}
	break;
	}


    }



}
